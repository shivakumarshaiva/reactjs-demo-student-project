/**
 * App
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import {Helmet} from 'react-helmet';
import styled from 'styled-components';
import {Redirect, Route, Switch} from 'react-router-dom';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { intersection } from '../../utils/commonFuctions';
import UnAuthorized from '../../components/UnAuthorized/index';
import theme from '../../theme';

import DashBoard from '../DashBoard';
import PageBase from '../../components/PageBase';

import Users from '../Users/List';
import CreateCreate from '../Users/Register';
import EditUsers from '../Users/Edit';
import Login from '../Login';
import RestPassword from '../ChangePassword';
import ForgotPassword from '../../containers/ForgotPassword';

import Students from '../Students/List';
import CreateStudent from '../Students/Create';
import EditStudent from '../Students/Edit';

import Teachers from '../Teachers/List';
import CreateTeacher from '../Teachers/Create';
import EditTeacher from '../Teachers/Edit';

const AppWrapper = styled.div`
  min-height: 100%;
`;

export var roles;
const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={
    (props) => {
      if (!localStorage.token && !localStorage.user) {
        return (<Redirect to={{pathname: '/login', state: {from: props.location }}} />);
      }
      const scopes = JSON.parse(localStorage.getItem('user')).role;
      roles = scopes;
      const intersectingRoles = intersection(scopes, rest.roles);
      if (intersectingRoles && intersectingRoles.length) {
        return (<Component userRoles={scopes} {...props} />);
      } else {
        return (<UnAuthorized />);
      }
    }
} />
);
const LoginContainer = () => (
  (localStorage.getItem('token')) ? 
  <Route path='' render={() => <Redirect to="/dashboard" />} />
  :
  <Route path='' render={() => <Redirect to="/login" />} />
)

export default function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <AppWrapper>
        <Helmet titleTemplate="Admin" defaultTitle="School Admin">
          <meta name="description" content="School Admin" />
        </Helmet>
        <Switch>
          <Route path="/reset" component={RestPassword} exact />
          <Route path="/login" component={Login} exact />
          <Route path="/forgot-password" component={ForgotPassword} exact />
          <PageBase>
            <PrivateRoute path='/dashboard' component={DashBoard} exact roles={['ROLE_ADMIN', 'ROLE_USER']}/>
            <PrivateRoute path='/users' component={Users} exact roles={['ROLE_ADMIN']}/>
            <PrivateRoute path='/register' component={CreateCreate} exact roles={['ROLE_ADMIN']}/>
            <PrivateRoute path='/users/:id/edit' component={EditUsers} exact roles={['ROLE_ADMIN']} />
            
            <PrivateRoute path='/students' component={Students} exact roles={['ROLE_ADMIN', 'ROLE_USER']} />
            <PrivateRoute path='/students/create' component={CreateStudent} exact roles={['ROLE_ADMIN', 'ROLE_USER']} />
            <PrivateRoute path='/students/:id/edit' component={EditStudent} exact roles={['ROLE_ADMIN', 'ROLE_USER']} />

            <PrivateRoute path='/teachers' component={Teachers} exact roles={['ROLE_ADMIN', 'ROLE_USER']} />
            <PrivateRoute path='/teachers/create' component={CreateTeacher} exact roles={['ROLE_ADMIN', 'ROLE_USER']} />
            <PrivateRoute path='/teachers/:id/edit' component={EditTeacher} exact roles={['ROLE_ADMIN', 'ROLE_USER']} />
            
            <Route exact path='' component={LoginContainer} />
          </PageBase>
        </Switch>   
      </AppWrapper>
    </MuiThemeProvider>
  );
}
