import React from 'react';
import PropTypes from 'prop-types';
import {compose} from 'redux';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';

import PageHeader, {HeaderButton} from '../../../components/PageHeader';
import AddIcon from '@material-ui/icons/Add';

import {createStructuredSelector} from 'reselect';
// import {selectsUsers, selectUserTotal,} from '../../../reducers/user/userSelectors';
import Table from '../../../components/Table'
import sagaHelper from '../../../sagas';
import {default as sagaInjector} from '../../../utils/injectSaga';
import UsersCard from '../../../components/Cards/UsersCard'
import EditIcon from '@material-ui/icons/Edit';
import NavigationBar from '../../../components/NavigationBar';
import Card from '@material-ui/core/Card';
import {phoneNumberFormater as phoneFormatter} from '../../../utils/commonFuctions';
import {deleteResource, list, RESOURCE_DELETE, RESOURCE_LIST, search, USERS_RESOURCE,} from '../../../actions';

import {DELETE_SUCCESS} from "../../../utils/constonts";
import {selectMessage} from '../../../reducers/genericSelector';

class UserList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      order: 'desc',
      orderBy: 'updatedAt',
      selected: [],
      page: 1,
      rowsPerPage: 5,
      searchText: '',
    };
  }

  componentDidMount() {
    this.getUsersList(({
      sort: `${this.state.orderBy},${this.state.order}`,
      page: `${this.state.page}`,
      size: `${this.state.rowsPerPage}`
    }));
  }
  getUsersList = (data = {}) => {
    // this.props.getUsersRequest(data);
  }

  createSubmit = () => {
    this.props.history.push('/register');
  }

  handleChangePage = (event, page) => {
    this.setState({ page: page });
    this.setState({selected: []})
    if(this.state.searchText) {
      this.props.makeSearchRequest({
        username : this.state.searchText,
        page: `${page}`,
        size: `${this.state.rowsPerPage}`,
        sort: `${this.state.orderBy},${this.state.order}`,
      });
    } else {
      this.props.makePageChangeRequest({
        page: `${page}`,
        size: `${this.state.rowsPerPage}`,
        sort: `${this.state.orderBy},${this.state.order}`,
      })
    }
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
    if (this.state.searchText) {
      this.props.makeSearchRequest({
        username: this.state.searchText,
        page: `${this.state.page}`,
        size: `${event.target.value}`
      });
    } else {
      this.getUsersList(({
        page: `${this.state.page}`,
        size: `${event.target.value}`,
        sort: `${this.state.orderBy},${this.state.order}`,
      }));
    }
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';
    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }
    this.setState({ order, orderBy }, () => this.props.makeSortingRequest({ 
      sort: `${property},${order}`,
      page: `${this.state.page}`,
      size: `${this.state.rowsPerPage}` 
    }));
  };

  makeSearchRequest = (event) => {
    event.preventDefault();
    this.props.makeSearchRequest({
      username: this.state.searchText,
      page: `${this.state.page}`,
      size: `${this.state.rowsPerPage}`
    });
  }

  makeDeleteAllRequest = (event) => {
    this.props.makeDeleteAllRequest({
      ids: this.state.selected.join(','),
      page: `${this.state.page}`,
      size: `${this.state.rowsPerPage}`
    })
    this.setState({ selected: [] });
  }

  handleSelectAllClick = (event, checked) => {
    const {users} = this.props;
    if (checked) {
      this.setState(state => ({ selected: users && users.map(n => n.id) }));
    } else {
      this.setState({ selected: [] });
    }
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected && selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    this.setState({ selected: newSelected });
  };

  handleFieldChange = (event) => {
    this.setState({
      searchText: event.target.value,
    });
  }

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { total, users, selectMessage } = this.props
    const { page, rowsPerPage, selected, orderBy, order, searchText } = this.state

    if (selectMessage && selectMessage.indexOf(DELETE_SUCCESS) > -1) {
      {
        this.props.getUsersRequest({
          page: `${page}`,
          size: `${rowsPerPage}`,
          sort: `${orderBy},${order}`,
        })
      }
    }

    let userList = users && users.map(suggestion => ({
      id: suggestion.id,
      employeeId: suggestion.employeeId,
      username: suggestion.username,
      firstName: suggestion.firstName,
      lastName: suggestion.lastName,
      contactNumber: phoneFormatter(suggestion.contactNumber),
    }))

    const tableData = {
      columns: [  
        { id: 'employeeId', numeric: false, disablePadding: false, label: 'Employee Id', enableSort: true },
        { id: 'username', numeric: false, disablePadding: false, label: 'User Name', enableSort: true },
        { id: 'firstName', numeric: false, disablePadding: true, label: 'First Name', enableSort: true },
        { id: 'lastName', numeric: false, disablePadding: false, label: 'Last Name', enableSort: true },
        { id: 'contactNumber', numeric: false, disablePadding: false, label: 'Contact Number', enableSort: false},
      ],
      edit: true,
      rowData: userList,
      path: '/users'
    };


    const createButton = <HeaderButton handler={this.createSubmit} icon={<AddIcon />} iconLabel="Create" toolTip="Create New User" />
    const pageHeader = <PageHeader title={"User List"} headerButtons={[]} />

    const actions = [
      { urlTemplate: "users/{id}/edit", icon: <EditIcon />, toolTip: "Edit" }
    ]

    return (
      <div>
        <NavigationBar
          isGoBack={true}
          isRefresh={true}
          headerButtons={[createButton]}
        />
        <Card>
          <Table
            actions={actions}
            pageHeader={pageHeader}
            tableData={tableData}
            total={total}
            handleChangePage={this.handleChangePage}
            handleChangeRowsPerPage={this.handleChangeRowsPerPage}
            page={page}
            rowsPerPage={rowsPerPage}
            handleSelectAllClick={this.handleSelectAllClick}
            isSelected={this.isSelected}
            handleClick={this.handleClick}
            handleRequestSort={this.handleRequestSort}
            selected={selected}
            orderBy={orderBy}
            order={order}
            handleFieldChange={this.handleFieldChange}
            makeSearchRequest={this.makeSearchRequest}
            searchText={searchText}
            deleteSelected={this.makeDeleteAllRequest}
          />
          {
            tableData && (tableData.rowData && (tableData.rowData.map(n => {
              return (
                <UsersCard key={n.id}
                  firstName={n.firstName}
                  lastName={n.lastName}
                  userName={n.username}
                  address={n.address && n.address.city}
                />);
            })))
          }
        </Card>
      </div>
    );
  }
}

UserList.propTypes = {
  users: PropTypes.array,
  total:PropTypes.number,
};

const mapDispatchToProps = (dispatch) => ({
  makePageChangeRequest: (param) => dispatch(list(USERS_RESOURCE, param)),
  makeSearchRequest: (param) => dispatch(search(USERS_RESOURCE, param)),
  makeDeleteAllRequest: (param) => dispatch(deleteResource(USERS_RESOURCE, param)),
  makeSortingRequest: (param) => dispatch(list(USERS_RESOURCE, param)),
  getUsersRequest: (param) => dispatch(list(USERS_RESOURCE, param)), 
});

const mapStateToProps = createStructuredSelector({
  // users: selectsUsers(),
  // total: selectUserTotal(),
  selectMessage: selectMessage(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  sagaInjector(sagaHelper(RESOURCE_LIST)),
  sagaInjector(sagaHelper(RESOURCE_DELETE)),
  withConnect,
)(withRouter(UserList));