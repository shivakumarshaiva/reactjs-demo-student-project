import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import Form from '../../../components/Form';
import {
    ACCESORIES_RESOURCE,
    getLookup,
    getOne,
    LOOKUP_RESOURCE,
    RESOURCE_GET_ONE,
    RESOURCE_LOOKUP,
    RESOURCE_UPDATE,
    update,
} from '../../../actions';
import {
  isStringIncludes,
  cleanDecimalNumber,
  cleanIntegerNumber,
} from '../../../utils/commonFuctions';

import {selectAccessory,} from '../../../reducers/accessory/accessorySelector';
import {createStructuredSelector} from 'reselect';
import sagaHelper from '../../../sagas';
import {default as sagaInjector} from '../../../utils/injectSaga';
import Card from '@material-ui/core/Card';
import PageHeader from '../../../components/PageHeader';
import ListIcon from '@material-ui/icons/List';
import NavigationBar from '../../../components/NavigationBar';

import {UPDATE_SUCCESS} from "../../../utils/constonts"
import {selectMessage} from '../../../reducers/genericSelector';
import Alert from '../../../components/Alert';
import {selectLookup,} from '../../../reducers/lookup/lookupSelector';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    width: 400,
    margin: '0 auto',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  menu: {
    width: 300,
  },
});

class AccessoryUpdate extends React.Component {
  constructor() {
    super();
    this.state = {
      id: '',
      sku: '',
      cost: '',
      unitPrice: '',
      title: '',
      description: '',
      logo: null,
      brand: '',
      vendor: '',
      vendors: [],
      file: null,
      isDilogOpen : false,
      dilogMessage : '',
      isTitleValid: null,
      showFormInvalid: false,
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.makeAccessoryGetRequest(id);
    this.props.makeGetLookupRequest();
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.accessory) {
      const {
        id, sku, cost, unitPrice, title, description, brand, vendor,
      } = nextProps.accessory;
      this.setState({
        id,
        sku,
        cost,
        unitPrice,
        title,
        description,
        brand,
        vendor: vendor && vendor.id,
        vendors: nextProps.lookup.vendors,
      })
    }
  }

  handleFieldChange = (fieldName, isfieldValid = null) => (event) => {
    if (fieldName === 'unitPrice' || fieldName === 'cost') {
      this.setState({
        [fieldName]: cleanDecimalNumber(event.target.value),
        ...(isfieldValid && { [isfieldValid]: Boolean(event.target.value) }),
      });
    } else {
      this.setState({
        [fieldName]: event.target.value,
        ...(isfieldValid && { [isfieldValid]: Boolean(event.target.value) }),
      });
    }
  }

  handleFormSubmit = (event) => {
    event.preventDefault();
    const {
      id, sku, cost, unitPrice, title, description, brand, logo, vendor,
    } = this.state;

    if (!sku) {
      this.setState({
        isDilogOpen: true,
        dilogMessage : 'Please Enter SKU Number'
      });
      return;
    }

    if (!title) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Title',
      });
      return;
    }

    const payload = {
      id,
      sku,
      cost,
      unitPrice,
      title,
      description,
      brand,
      vendor: {
        id: vendor,
      },
    };

    const formData = new FormData();
    { logo && formData.append('image', logo) }
    formData.append(ACCESORIES_RESOURCE, JSON.stringify(payload));
    this.props.makeAccessoryUpdateRequest(formData, this.props.history, true);
  }

  onChange = (e) => {
    this.setState({ logo: e.target.files[0] })
  }

  snackClose = () => {
    this.setState({ open: false });
  };
  accessoryListPage = () => {
    this.props.history.push('/accessories')
  }
  dilogHandle = () => {
    this.setState({
      isDilogOpen: false,
    });
  }

  render() {
    const { 
      sku, cost, unitPrice, title, description, brand, isTitleValid, dilogMessage, isDilogOpen, vendor, vendors
    } = this.state;

    const { selectMessage } = this.props
    const isSuccess = isStringIncludes(selectMessage, UPDATE_SUCCESS)

    if (isSuccess) {
        this.props.history.push("/accessories");
    }

    let vendorList = vendors && vendors.map(suggestion => ({
      displayName: suggestion.name,
      name: suggestion.id,
    }))

    const formProps = [
      //product details
      { type: 'text', label: 'Title', value: title, isValid: isTitleValid, fieldName: 'title', validFieldName: 'isTitleValid', isLeft: true },
      { type: 'text', label: 'SKU', value: sku, fieldName: 'sku', isLeft: true },
      { type: 'text', label: 'Unit Price', value: unitPrice, fieldName: 'unitPrice', isLeft: true },
      { type: 'text', label: 'Cost', value: cost, fieldName: 'cost', isLeft: true },
      { type: 'text', label: 'Description', value: description, fieldName: 'description', isLeft: true },
      { type: 'text', label: 'Brand', value: brand, fieldName: 'brand', isLeft: true },
      { type: 'select', label: 'Vendor', value: vendor, fieldName: 'vendor', menuItems: vendorList, helperText: "Select Vendor", isLeft: true },
    ];

    const actions = [
      {
        toolTip: "Accessory List",
        icon: <ListIcon />,
        method: this.accessoryListPage,
      }
    ]

    const alert = <Alert isDilogOpen={isDilogOpen} dilogMessage={dilogMessage} dilogHandle={this.dilogHandle} />
   
    return (
      <div>
        {isDilogOpen && alert}
        <NavigationBar
          isGoBack={true}
          isRefresh={true}
          actions={actions}
        />
        <Card>
          <PageHeader title={"Update Accessory"} headerButtons={[]} />
          <Form
            formProps={formProps}
            handleFieldChange={this.handleFieldChange}
            isUpdate={true}
            handleFormSubmit={this.handleFormSubmit}
            onChange={this.onChange}
            leftTitle={"Accessory Details"}
          />
        </Card>
      </div>
    );
  }
}

AccessoryUpdate.propTypes = {
  accessory:PropTypes.object
};

const mapDispatchToProps = dispatch => ({
  makeGetLookupRequest: () => dispatch(getLookup(LOOKUP_RESOURCE)),
  makeAccessoryGetRequest: (id) => dispatch(getOne(ACCESORIES_RESOURCE, id)),
  makeAccessoryUpdateRequest: (payload, history, isMultipart) => dispatch(update(ACCESORIES_RESOURCE, payload, history, isMultipart)),
});

const mapStateToProps = createStructuredSelector({
  accessory: selectAccessory(),
  selectMessage: selectMessage(),
  lookup: selectLookup(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  sagaInjector(sagaHelper(RESOURCE_UPDATE)),
  sagaInjector(sagaHelper(RESOURCE_GET_ONE)),
  sagaInjector(sagaHelper(RESOURCE_LOOKUP)),
  withConnect,
)(withStyles(styles)(withRouter(AccessoryUpdate)));
