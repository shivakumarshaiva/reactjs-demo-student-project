import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'react-router-redux';
import { createLogger } from 'redux-logger';
import rootReducer from '../reducers';
import createHistory from 'history/createBrowserHistory';
const history = createHistory();
const sagaMiddleware = createSagaMiddleware();
const loggerMiddleware = createLogger();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const enhancer = composeEnhancers(
  applyMiddleware(
    sagaMiddleware,
    loggerMiddleware,
    routerMiddleware(history)
  )
);

const store = createStore(rootReducer, enhancer);

store.runSaga = sagaMiddleware.run;
export default function configureStore(preloadedState) {
  return store;
}