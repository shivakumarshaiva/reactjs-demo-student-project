import React from 'react';

class UnAuthorized extends React.Component {

  render() {
    return (
      <div style={{textAlign:'center'}}>
        <img src='/unauth.png' alt="UnAuthorized"  />
      </div>
    );
  }
}

export default UnAuthorized;
