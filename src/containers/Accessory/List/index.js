import React from 'react';
import PropTypes from 'prop-types';
import {compose} from 'redux';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {selectAccessories, selectAccessoriesTotal,} from '../../../reducers/accessory/accessorySelector';
import Table from '../../../components/Table';
import sagaHelper from '../../../sagas';
import {default as sagaInjector} from '../../../utils/injectSaga';

import PageHeader, {HeaderButton} from '../../../components/PageHeader';
import AddIcon from '@material-ui/icons/Add';
import Card from '@material-ui/core/Card';
import EditIcon from '@material-ui/icons/Edit';
import { currencyFormatter as formatter, isStringIncludes} from '../../../utils/commonFuctions';
import {ACCESORIES_RESOURCE, deleteResource, list, RESOURCE_DELETE, RESOURCE_LIST, search,} from '../../../actions';
import NavigationBar from '../../../components/NavigationBar';
import {DELETE_SUCCESS} from "../../../utils/constonts";
import {selectMessage} from '../../../reducers/genericSelector';

class AccessoryList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      order: 'desc',
      orderBy: 'updatedAt',
      selected: [],
      page: 0,
      rowsPerPage: 5,
      searchText: '',
    };
  }

  componentDidMount() {
    this.getAccessoriesList(({
      sort: `${this.state.orderBy},${this.state.order}`,
      page: `${this.state.page}`,
      size: `${this.state.rowsPerPage}`
    }));
  }
  getAccessoriesList = (data = {}) => {
    this.props.getAccessoriesRequest(data);
  }

  createSubmit = () => {
    this.props.history.push('/accessories/create');
  }

  handleChangePage = (event, page) => {
    this.setState({ page: page });
    this.setState({selected: []})
    if(this.state.searchText) {
      this.props.makeSearchRequest({
        sort: `${this.state.orderBy},${this.state.order}`,
        title: this.state.searchText,
        page: `${page}`,
        size: `${this.state.rowsPerPage}`,
      });
    } else {
      this.props.makePageChangeRequest({
        sort: `${this.state.orderBy},${this.state.order}`,
        page: `${page}`,
        size: `${this.state.rowsPerPage}`,
      })
    }
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
    if (this.state.searchText) {
      this.props.makeSearchRequest({
        sort: `${this.state.orderBy},${this.state.order}`,
        title: this.state.searchText,
        page: `${this.state.page}`,
        size: `${event.target.value}`,
      });
    } else {
      this.getAccessoriesList(({
        sort: `${this.state.orderBy},${this.state.order}`,
        page: `${this.state.page}`,
        size: `${event.target.value}`,
      }));
    }
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';
    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }
    this.setState({ order, orderBy }, () => this.props.makeSortingRequest({ 
      sort: `${property},${order}`,
      page: `${this.state.page}`,
      size: `${this.state.rowsPerPage}` 
    }));
  };

  makeSearchRequest = (event) => {
    event.preventDefault();
    this.props.makeSearchRequest({
      title: this.state.searchText,
      page: `${this.state.page}`,
      size: `${this.state.rowsPerPage}`
    });
  }

  makeDeleteAllRequest = (event) => {
    this.props.makeDeleteAllRequest({
      ids: this.state.selected.join(','),
      page: `${this.state.page}`,
      size: `${this.state.rowsPerPage}`
    })
    this.setState({ selected: [] });
  }

  handleSelectAllClick = (event, checked) => {
    const { accessories} = this.props;
    if (checked) {
      this.setState(state => ({ selected: accessories && accessories.map(n => n.id) }));
    } else {
      this.setState({ selected: [] });
    }
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    this.setState({ selected: newSelected });
  };

  handleFieldChange = (event) => {
    this.setState({
      searchText: event.target.value,
    });
  }

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { total, accessories, selectMessage } = this.props
    const { page, rowsPerPage, selected, orderBy, order, searchText } = this.state
    const isSuccess = isStringIncludes(selectMessage, DELETE_SUCCESS);

    if (isSuccess) {
      {
        this.props.getAccessoriesRequest({
          page: `${page}`,
          size: `${rowsPerPage}`,
          sort: `${orderBy},${order}`,
        })
      }
    }

    let accessoryList = accessories && accessories.map(suggestion => ({
      id: suggestion.id,
      title: suggestion.title,
      sku: suggestion.sku,
      brand: suggestion.brand,
      unitPrice: formatter.format(suggestion.unitPrice),
      cost: suggestion.cost ? formatter.format(suggestion.cost) : formatter.format(0),
    }))

    const tableData = {
      columns: [  
        { id: 'title', numeric: false, disablePadding: true, label: 'Title', enableSort: true },
        { id: 'sku', numeric: false, disablePadding: false, label: 'SKU', enableSort: false },
        { id: 'brand', numeric: false, disablePadding: false, label: 'Brand', enableSort: false },
        { id: 'unitPrice', numeric: true, disablePadding: false, label: 'Unit Price', enableSort: true },
        { id: 'cost', numeric: true, disablePadding: false, label: 'Cost', enableSort: true },
      ],
      edit: true,
      rowData: accessoryList,
      path: ACCESORIES_RESOURCE,
    };

    const createButton = <HeaderButton key="createButton" handler={this.createSubmit} icon={<AddIcon />} iconLabel="Create" toolTip="Create New Accessory" />
    const pageHeader = <PageHeader title={"Accessory List"} headerButtons={[]} />

    const actions = [
      { urlTemplate: "accessories/{id}/edit", icon: <EditIcon />, toolTip: "Edit" }
    ]

    return (
      <div>
        <NavigationBar
          isGoBack={true}
          isRefresh={true}
          headerButtons={[createButton]}
        />
        <Card>
          <Table
            actions={actions}
            pageHeader={pageHeader}
            tableData={tableData}
            total={total}
            handleChangePage={this.handleChangePage}
            handleChangeRowsPerPage={this.handleChangeRowsPerPage}
            page={page}
            rowsPerPage={rowsPerPage}
            handleSelectAllClick={this.handleSelectAllClick}
            isSelected={this.isSelected}
            handleClick={this.handleClick}
            handleRequestSort={this.handleRequestSort}
            selected={selected}
            orderBy={orderBy}
            order={order}
            handleFieldChange={this.handleFieldChange}
            makeSearchRequest={this.makeSearchRequest}
            searchText={searchText}
            deleteSelected={this.makeDeleteAllRequest}
          />
        </Card>
      </div>
      
    );
  }
}

AccessoryList.propTypes = {
  accessories: PropTypes.array,
  total:PropTypes.number,
  isLoading: PropTypes.bool,
};

const mapDispatchToProps = (dispatch) => ({
  makePageChangeRequest: (param) => dispatch(list(ACCESORIES_RESOURCE, param)),
  makeSearchRequest: (param) => dispatch(search(ACCESORIES_RESOURCE, param)),
  makeDeleteAllRequest: (param) => dispatch(deleteResource(ACCESORIES_RESOURCE, param)),
  makeSortingRequest: (param) => dispatch(list(ACCESORIES_RESOURCE, param)),
  getAccessoriesRequest: (param) => dispatch(list(ACCESORIES_RESOURCE, param)), 
});

const mapStateToProps = createStructuredSelector({
  accessories: selectAccessories(),
  total: selectAccessoriesTotal(),
  selectMessage: selectMessage(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  sagaInjector(sagaHelper(RESOURCE_LIST)),
  sagaInjector(sagaHelper(RESOURCE_DELETE)),
  withConnect,
)(withRouter(AccessoryList));