import { createSelector } from 'reselect';

const root = state => {
  return state.root.global;
}

const selectFetching = () =>
  createSelector(root, rootState => rootState.isFetching);

const selectMessage = () =>
  createSelector(root, rootState => rootState.message);

const selectIsTokenExpired = () =>
  createSelector(root, rootState => rootState.isTokenExpired);

const selectStatusCode = () =>
  createSelector(root, rootState => rootState.statusCode);

  

export {
  selectFetching,
  selectMessage,
  selectStatusCode,
  selectIsTokenExpired,
};
