import React from 'react';
import PropTypes from 'prop-types';
import {compose} from 'redux';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import PageHeader, {HeaderButton} from '../../../components/PageHeader';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import Card from '@material-ui/core/Card';
import {selectStudents, selectStudentsTotal} from '../../../reducers/students/studentsSelector';
import Table from '../../../components/Table'
import sagaHelper from '../../../sagas';
import {default as sagaInjector} from '../../../utils/injectSaga';
import { 
  STUDENTS_RESOURCE, 
  deleteResource, 
  list, 
  studenntlist, 
  RESOURCE_DELETE, 
  STUDENT_RESOURCE_LIST,
  STUDENT_RESOURCE_SEARCH,
  studenntSearch,
} from '../../../actions';
import NavigationBar from '../../../components/NavigationBar';
import {DELETE_SUCCESS} from "../../../utils/constonts";
import {selectMessage} from '../../../reducers/genericSelector';
import { isStringIncludes } from '../../../utils/commonFuctions';

class CustomerList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      order: 'desc',
      orderBy: 'updatedAt',
      selected: [],
      page: 1,
      rowsPerPage: 5,
      searchText: '',
      standard: 1,
      section: 'A'
    };
  }

  componentDidMount() {
    this.getStudentsList(({
      sort: `${this.state.orderBy},${this.state.order}`,
      page: `${this.state.page}`,
      limit: `${this.state.rowsPerPage}`
    }));
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selected:[],
      searchText: '',
    })
  }
  getStudentsList = (data = {}) => {
    this.props.getStudentsRequest(data);
  }

  createSubmit = () => {
    this.props.history.push('/students/create');
  }
  uploadSubmit = () => {
    this.props.history.push('/students/upload');
  }

  handleChangePage = (event, page) => {
    
    if (page > 0 ){
      this.setState({ page: page });
      this.setState({ selected: [] })
    }
    
    if(this.state.searchText) {
      this.props.makeSearchRequest({
        firstName: this.state.searchText,
        page: `${page > 0 ? page : 1}`,
        limit: `${this.state.rowsPerPage}`,
        sort: `${this.state.orderBy},${this.state.order}`,
      });
    } else if (page && page > 0){
      this.props.makePageChangeRequest({
        page: `${page > 0 ? page : 1}`,
        limit: `${this.state.rowsPerPage}`,
        sort: `${this.state.orderBy},${this.state.order}`,
      })
    }
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
    if (this.state.searchText) {
      this.props.makeSearchRequest({
        firstName: this.state.searchText,
        page: `${this.state.page}`,
        limit: `${event.target.value}`,
        sort: `${this.state.orderBy},${this.state.order}`,
      });
    } else {
      this.getStudentsList(({
        page: `${this.state.page}`,
        limit: `${event.target.value}`,
        sort: `${this.state.orderBy},${this.state.order}`,
      }));
    }
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';
    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }
    this.setState({ order, orderBy }, () => this.props.makeSortingRequest({ 
      sort: `${property},${order}`,
      page: `${this.state.page}`,
      limit: `${this.state.rowsPerPage}` 
    }));
  };

  makeSearchRequest = (event) => {
    event.preventDefault();
    this.props.makeSearchRequest({
      firstName: this.state.searchText,
      page: `${this.state.page}`,
      limit: `${this.state.rowsPerPage}`
    });
  }

  makeDeleteAllRequest = (event) => {
    this.props.makeDeleteAllRequest({
      ids: this.state.selected.join(','),
      page: `${this.state.page}`,
      limit: `${this.state.rowsPerPage}`
    })
    this.setState({ selected: [] });
    this.props.history.push('/students');
  }

  handleSelectAllClick = (event, checked) => {
    const { students} = this.props;
    if (checked) {
      this.setState(state => ({ selected: students && students.map(n => n.id) }), () => console.log("selected all items", this.state.selected));
    } else {
      this.setState({ selected: [] }, () => console.log("unselected items", this.state.selected));
    }
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected && selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    this.setState({ selected: newSelected }, () => console.log("selected customs item", this.state.selected));
  };

  handleFieldChange = (fieldName, isfieldValid = null) => (event) => {
    this.setState({
      [fieldName]: event.target.value,
      ...(isfieldValid && { [isfieldValid]: Boolean(event.target.value) }),
    });
  }

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { total, students, selectMessage } = this.props
    const { page, rowsPerPage, selected, orderBy, order, searchText, standard, section } = this.state

    const isSuccess = isStringIncludes(selectMessage, DELETE_SUCCESS) 

    if (isSuccess) {
      this.props.getStudentsRequest({
        page: `${page > 0 ? page : 1}`,
        limit: `${rowsPerPage}`,
        sort: `${orderBy},${order}`,
      })
    }

    let studentsList = students && students.map(suggestion => ({
      id: suggestion._id,
      firstName: suggestion.firstName,
      class: suggestion.class,
      section: suggestion.section,
      admissionNumber: suggestion.admissionNumber,
      rollNumber: suggestion.rollNumber,
      dateOfAdmission: suggestion.dateOfAdmission,
    }))

    const tableData = {
      columns: [  
        { id: 'firstName', numeric: false, disablePadding: true, label: 'Name', enableSort: true },
        { id: 'class', numeric: false, disablePadding: true, label: 'Class', enableSort: true },
        { id: 'section', numeric: false, disablePadding: false, label: 'Section', enableSort: false },
        { id: 'admissionNumber', numeric: false, disablePadding: false, label: 'A-Number', enableSort: false },
        { id: 'rollNumber', numeric: false, disablePadding: false, label: 'R-Number', enableSort: false },
        { id: 'dateOfAdmission', numeric: false, disablePadding: false, label: 'D-O-A', enableSort: false },
      ],
      edit: true,
      rowData: studentsList,
      path: STUDENTS_RESOURCE,
    };

    const actions = [
      { urlTemplate: "students/{id}/edit", icon: <EditIcon />, toolTip: "Edit" }
    ]

    const createButton = <HeaderButton key="createButton" handler={this.createSubmit} icon={<AddIcon />} iconLabel="Create" toolTip="Create New Student" />
    const pageHeader = <PageHeader title={"Students List"} headerButtons={[] } />

    return (
      <div>
        <NavigationBar
          isGoBack={true}
          isRefresh={true}
          headerButtons={[createButton]}
        />
        <Card>
          <Table
            standard={standard}
            section={section}
            actions={actions}
            pageHeader={pageHeader}
            tableData={tableData}
            total={total}
            handleChangePage={this.handleChangePage}
            handleChangeRowsPerPage={this.handleChangeRowsPerPage}
            page={page}
            rowsPerPage={rowsPerPage}
            handleSelectAllClick={this.handleSelectAllClick}
            isSelected={this.isSelected}
            handleClick={this.handleClick}
            handleRequestSort={this.handleRequestSort}
            selected={selected}
            orderBy={orderBy}
            order={order}
            handleFieldChange={this.handleFieldChange}
            makeSearchRequest={this.makeSearchRequest}
            searchText={searchText}
            deleteSelected={this.makeDeleteAllRequest}
          />
        </Card>
      </div>
      
    );
  }
}

CustomerList.propTypes = {
  students: PropTypes.array,
  total:PropTypes.number,
  isLoading: PropTypes.bool,
};

const mapDispatchToProps = (dispatch) => ({
  makePageChangeRequest: (param) => dispatch(studenntlist(STUDENTS_RESOURCE, param)),
  makeSearchRequest: (param) => dispatch(studenntSearch(STUDENTS_RESOURCE, param)),
  makeDeleteAllRequest: (param) => dispatch(deleteResource(STUDENTS_RESOURCE, param)),
  makeSortingRequest: (param) => dispatch(list(STUDENTS_RESOURCE, param)),
  getStudentsRequest: (param) => dispatch(studenntlist(STUDENTS_RESOURCE, param)),
});

const mapStateToProps = createStructuredSelector({
  students: selectStudents(),
  total: selectStudentsTotal(),
  selectMessage: selectMessage(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  sagaInjector(sagaHelper(STUDENT_RESOURCE_LIST)),
  sagaInjector(sagaHelper(STUDENT_RESOURCE_SEARCH)),
  sagaInjector(sagaHelper(RESOURCE_DELETE)),
  withConnect,
)(withRouter(CustomerList));