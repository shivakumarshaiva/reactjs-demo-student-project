import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

const pageHeaderStyles = theme => ({
  spacer: {
    flex: '1 1 100%',
  },
  regular: {
    paddingLeft: '0',
    paddingRight: '0',
    minHeight: 45,
    backgroundColor: theme.palette.primary.main,
  },
  font: {
    fontSize: '1.5rem',
    fontWeight: 400,
    marginLeft:10,
    color: '#fff',
    fontFamily: theme.typography.fontFamily,
    width: '100%'
  },
  button: {
    color: '#fff',
    borderRadius: '5px',
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.light,
    },
  },
});

export const HeaderButton = (props) => {
  const { toolTip, handler, icon, iconLabel,  } = props;
  return (
    <div>
      <Tooltip title={toolTip}>
        <Button onClick={handler}>
          {icon} {iconLabel}
        </Button>
      </Tooltip>
    </div>
  )
}

let PageHeader = props => {
  const { classes, title, headerButtons, textField, iconHandling } = props;
  return (
      <Toolbar className={classes.regular}>
        <Typography variant="title" className={classes.font}>
          {title}
          {
            textField &&
            <TextField
              text
              style={{width:'10%', color:'#fff'}}
              disabled={textField.isDisabled}
              label={textField.lable}
              value={textField.value}
              onChange={textField.handleFieldChange(textField.field)}
              className={classes.textField}
              helperText={textField.helperText}
            />
          }
          {
            iconHandling && 
            <Tooltip title={iconHandling.toolTip}>
                <Button
                  className={classes.button}
                  key={1}
                  onClick={iconHandling.method}
                >
                  {iconHandling && iconHandling.icon}
                </Button>
            </Tooltip>
          }
        </Typography>            
        {headerButtons}
      </Toolbar>
  );
};

PageHeader.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(pageHeaderStyles, { withTheme: true })(PageHeader);