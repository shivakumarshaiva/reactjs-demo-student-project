import { takeLatest, put, call } from 'redux-saga/effects';
import { REQUEST, FAILURE, SUCCESS } from '../actions/actionUtil';
import client from '../services/api';

export default function sagaHelper(action) {
  return function* watcherSaga() {
    yield takeLatest(action[REQUEST], workerSaga, action[SUCCESS], action[FAILURE]);
  }
}

// worker saga: makes the api call when watcher saga sees the action
function* workerSaga(success, failure, action) {

    try {
        const response = yield call(client, action.resource, action.request.type, action.request.payload, action.isMultipart);

        if (response.status >= 200 || response.status < 300) {
            yield put({ type: success, response, resource: action.resource, });
        } else {
            yield put({ type: failure, response, resource: action.resource, });
        }

    } catch (error) {
        yield put({ type: failure, error, resource: action.resource,});
    }
}
