import * as Actions from '../actions';
import {FAILURE, REQUEST, SUCCESS} from '../actions/actionUtil';

// reducer with initial state
export const initialState = {
  users: {
    list: [],
    single: {},
    resource: null,
    total: 0,
  },
  students: {
    list: [],
    resource: null,
    total: 0,
    single: {},
  },
  teachers: {
    list: [],
    resource: null,
    total: 0,
    single: {},
  },
  global : {
    isFetching: false,
    message: '',
    isTokenExpired: false,
    statusCode:'',
  }
 
};

export default function genericReducer(state = initialState, action) {
  
  let data = null;
  let enhancedState = {};
  let enhanceGlobal = {};
  let finalState = null;

  switch (action.type) {

    case Actions.RESOURCE_CREATE[REQUEST]:
      enhanceGlobal['global'] = {
        isFetching: true,
      }
      finalState = { ...state, ...enhanceGlobal, };
      return finalState;

    case Actions.RESOURCE_CREATE[SUCCESS]:
      data = action.response.data;
      enhancedState[action.resource] = {
        single: data.data,
      }
      enhanceGlobal['global'] = {
        isFetching: false,
        statusCode: action.response.data.code,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, ...enhancedState };
      return finalState;

    case Actions.RESOURCE_CREATE[FAILURE]:
      data = action.error;
      enhanceGlobal['global'] = {
        isFetching: false,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, };
      return finalState;

    ////

    case Actions.RESOURCE_UPDATE[REQUEST]:
      enhanceGlobal['global'] = {
        isFetching: true
      }
      finalState = { ...state, ...enhanceGlobal, };
      return finalState;

    case Actions.RESOURCE_UPDATE[SUCCESS]:
      data = action.response.data;
      enhancedState[action.resource] = {
        single: data.data,
      }
      enhanceGlobal['global'] = {
        isFetching: false,
        statusCode: action.response.data.code,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, ...enhancedState };
      return finalState;

    case Actions.RESOURCE_UPDATE[FAILURE]:
      data = action.error;
      enhanceGlobal['global'] = {
        isFetching: false,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, };
      return finalState;

      ////

    case Actions.RESOURCE_UPDATE[REQUEST]:
      enhanceGlobal['global'] = {
        isFetching: true
      }
      finalState = { ...state, ...enhanceGlobal, };
      return finalState;

    case Actions.RESOURCE_UPDATE[SUCCESS]:
      data = action.response.data;
      enhancedState[action.resource] = {
        single: data.data,
      }
      enhanceGlobal['global'] = {
        isFetching: false,
        statusCode: action.response.data.code,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, ...enhancedState };
      return finalState;

    case Actions.RESOURCE_UPDATE[FAILURE]:
      data = action.error;
      enhanceGlobal['global'] = {
        isFetching: false,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, };
      return finalState;

      ////

    case Actions.RESOURCE_GET_ONE[REQUEST]:
      enhanceGlobal['global'] = {
        isFetching: true
      }
      finalState = { ...state, ...enhanceGlobal, };
      return finalState;

    case Actions.RESOURCE_GET_ONE[SUCCESS]:
      data = action.response.data;
      enhancedState[action.resource] = {
        single: data.data,
      }
      enhanceGlobal['global'] = {
        isFetching: false,
        statusCode: action.response.data.code,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, ...enhancedState };
      return finalState;

    case Actions.RESOURCE_GET_ONE[FAILURE]:
      data = action.error;
      enhanceGlobal['global'] = {
        isFetching: false,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, };
      return finalState;

    ////

    case Actions.RESOURCE_LIST[REQUEST]:
      enhanceGlobal['global'] = {
        isFetching: true,
      }
      finalState = { ...state, ...enhanceGlobal, };
    return finalState;

    case Actions.RESOURCE_LIST[SUCCESS]:
      data = action.response.data;
      enhancedState[action.resource] = {
        list: data.data.docs,
        total: data.data.total,
      }
      enhanceGlobal['global'] = {
        isFetching: false,
        statusCode: action.response.data.code,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, ...enhancedState };
    return finalState;

    case Actions.RESOURCE_LIST[FAILURE]:
      data = action.error;
      enhanceGlobal['global'] = {
        isFetching: false,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, };
    return finalState;

      ////

    case Actions.RESOURCE_DELETE[REQUEST]:
      enhanceGlobal['global'] = {
        isFetching: true
      }
      finalState = { ...state, ...enhanceGlobal,};
    return finalState;
  
    case Actions.RESOURCE_DELETE[SUCCESS]:
      data = action.response.data;
      enhancedState[action.resource] = {
        single: data.data,
      }
      enhanceGlobal['global'] = {
        isFetching: false,
        statusCode: action.response.data.code,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, ...enhancedState };
    return finalState;

    case Actions.RESOURCE_DELETE[FAILURE]:
      data = action.error;
      enhanceGlobal['global'] = {
        isFetching: false,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, };
    return finalState;

    ///student list

    case Actions.STUDENT_RESOURCE_LIST[REQUEST]:
      enhanceGlobal['global'] = {
        isFetching: true,
      }
      finalState = { ...state, ...enhanceGlobal, };
      return finalState;

    case Actions.STUDENT_RESOURCE_LIST[SUCCESS]:
      data = action.response.data;
      enhancedState[action.resource] = {
        list: data.data.docs,
        total: data.data.total,
      }
      enhanceGlobal['global'] = {
        isFetching: false,
        statusCode: action.response.data.code,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, ...enhancedState };
      return finalState;

    case Actions.STUDENT_RESOURCE_LIST[FAILURE]:
      data = action.error;
      enhanceGlobal['global'] = {
        isFetching: false,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, };
      return finalState;
    
      /// student search

    case Actions.STUDENT_RESOURCE_SEARCH[REQUEST]:
      enhanceGlobal['global'] = {
        isFetching: true,
      }
      finalState = { ...state, ...enhanceGlobal, };
      return finalState;

    case Actions.STUDENT_RESOURCE_SEARCH[SUCCESS]:
      data = action.response.data;
      enhancedState[action.resource] = {
        list: data.data.docs,
        total: data.data.total,
      }
      enhanceGlobal['global'] = {
        isFetching: false,
        statusCode: action.response.data.code,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, ...enhancedState };
      return finalState;

    case Actions.STUDENT_RESOURCE_SEARCH[FAILURE]:
      data = action.error;
      enhanceGlobal['global'] = {
        isFetching: false,
        message: data.message,
      }
      finalState = { ...state, ...enhanceGlobal, };
      return finalState;
      
    default:
      return state;
  }
}
