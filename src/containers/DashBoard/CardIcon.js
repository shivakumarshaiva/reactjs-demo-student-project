import React from 'react';
import Card from '@material-ui/core/Card';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    card: {
        float: 'left',
        margin: '-25px 20px 0 15px',
        zIndex: 100,
        borderRadius: 3,
    },
    icon: {
        width: 45,
        height: 54,
        paddingLeft: 22,
        color: '#fff',
    },
    font: {
        paddingRight: '10px',
        fontSize: '1.7rem',
        fontWeight: 400,
        marginLeft: '10px',
        color: theme.palette.primary.main,
        fontFamily: theme.typography.fontFamily,
        width: '100%'
    },
});

const CardIcon = ({ content, Icon, classes, bgColor }) => (
    <Card className={classes.card} style={{ backgroundColor: bgColor }}>
        {Icon && <Icon className={classes.icon} />}
        <span className={ classes.font}>
            {content && content}
        </span>
    </Card>
);

export default withStyles(styles, { withTheme: true })(CardIcon);