import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import classNames from 'classnames';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: '0px',
  },
  paperBody: {
    color: theme.palette.text.secondary,
    boxShadow: 'none',
  },
  container: {
    display: 'flex',
    marginLeft: '20px',
    width: '50%',
    '@media (max-width: 600px)': {
      width: '100%',
      flexWrap: 'wrap',
      marginRight: '20px',
      display: 'block',
    },
   
  },
  textField: {
    marginTop: '4px',
    marginBottom: '2px',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    '@media (max-width: 600px)': {
      width: '270px',
    },
    width: 500,
  },
  
  font: {
    fontSize: '1.5rem',
    fontWeight: 400,
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamily,
    flexGrow: 1,
  },

  title: {
    fontSize: '1rem',
    fontWeight: 400,
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamily,
    flexGrow: 1,
    marginTop:'10px',
  },

  photoFont: {
    fontSize: '.8rem',
    fontWeight: 400,
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamily,
    flexGrow: 1,
    marginTop: '10px',
  },
  errorFont: {
    fontSize: '1',
    fontWeight: 400,
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamily,
    flexGrow: 1,
  },
  button: {
    margin: '15px 0',
    color: '#fff',
    borderRadius: '5px',
    marginLeft: '25px',
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
  },
});

const Form = ({
  classes, formProps, handleFieldChange, handleFormSubmit, isUpdate, isFileUpload, fileTitle, onChange, 
  leftTitle, rightTitle, resource, switchAction
}) => (
    <div className={classes.root}>
      <Grid spacing={24}>
        <Grid item xs={12} style={{ padding: '0px' }}>
          <Card className={classes.paperBody}>
            <form className={classes.container} noValidate autoComplete="off">
              <div >
                <div>
                  <Typography variant="title" className={classes.title}>
                    {leftTitle}
                  </Typography>  
                  {
                    formProps
                      // .slice(0, Math.ceil(formProps.length / columns))
                      .map((formField, i) => {
                        return ( formField.isLeft && 
                          ( formField.type === 'text' ?
                              <div>
                                <TextField
                                  type={formField.type}
                                  label={formField.label}
                                  value={formField.value}
                                  onChange={handleFieldChange(formField.fieldName, formField.validFieldName)}
                                  className={classes.textField}
                                  fullWidth
                                  margin="normal"
                                  error={formField.isValid === false}
                                />
                                {
                                  formField.isValid === false &&
                                    <FormHelperText className={classes.red} id="name-error-text" >
                                      <span className={classes.errorFont}>
                                        Required Field
                                      </span>
                                    </FormHelperText> 
                                }
                              </div>
                              :
                              <TextField
                                select
                                label={!formField.value && formField.helperText}
                                className={classes.textField}
                                value={formField.value}
                                onChange={handleFieldChange(formField.fieldName)}
                                helperText= {formField.helperText}
                                margin="normal"
                              >
                                {
                                  formField.menuItems && formField.menuItems.map((role, i) =>
                                    <MenuItem key={i.name} value={role.name}>
                                      {role.displayName}
                                    </MenuItem>
                                  )
                                }
                              </TextField>
                            )
                        );
                    })
                    
                  }
                  {switchAction &&
                    <div>
                      <FormControlLabel
                        label={switchAction.lable}
                        control={
                          <Switch checked={switchAction.isEnabled} onChange={switchAction.activeControle} aria-label="active" />
                        }
                      />
                    </div>
                  }
                </div>
              </div>

              <div>
                <Typography variant="title" className={classes.title}>
                  {rightTitle}
                </Typography>
                {
                  formProps
                    // .slice(0, Math.ceil(formProps.length / columns))
                    .map((formField, i) => {
                      return (!formField.isLeft &&
                        (formField.type === 'text' ?
                          <div>
                            <TextField
                              type={formField.type}
                              label={formField.label}
                              value={formField.value}
                              onChange={handleFieldChange(formField.fieldName, formField.validFieldName)}
                              className={classes.textField}
                              fullWidth
                              margin="normal"
                              error={formField.isValid === false}
                            />
                            {
                              formField.isValid === false &&
                              <FormHelperText className={classes.red} id="name-error-text" >
                                <span className={classes.errorFont}>
                                  Required Field
                                      </span>
                              </FormHelperText>
                            }
                          </div>
                          :
                          <TextField
                            select
                            label={!formField.value && formField.helperText}
                            className={classes.textField}
                            value={formField.value}
                            onChange={handleFieldChange(formField.fieldName)}
                            helperText={formField.helperText}
                            margin="normal"
                          >
                            {
                              formField.menuItems && formField.menuItems.map((role, i) =>
                                <MenuItem key={i.name} value={role.name}>
                                  {role.displayName}
                                </MenuItem>
                              )
                            }
                          </TextField>
                        )
                      );
                    })
                }
                {isFileUpload &&
                  <div style={{ margin: '10px' ,}}>
                    <div className={classes.photoFont}>
                      {fileTitle}
                    </div>
                    <input type="file" onChange={onChange}/>
                  </div>
                }
              </div>
            </form>
           
            <Button
              key={1}
              className={classes.button}
              type="submit"
              onClick={handleFormSubmit}>
              <SaveIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
              {isUpdate ? 'Update' : 'Save'}
            </Button>
          </Card>
        </Grid>
      </Grid>
    </div>
  );

export default withStyles(styles)(Form);
