import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    minWidth: 275,
    '@media (min-width: 481px)': {
      display: 'none',
    },
    marginTop: 10,
    marginBottom: 5,
  },
  first: {
    fontSize: 20,
  },
  last: {
    fontSize: 15,
  },
  pos: {
    marginBottom: 0,
  },
};

function SimpleCard(props) {
  const { classes, firstName, lastName, userName, address, } = props;
            
  return (
    <div>
      <Card className={classes.card}>
        <CardContent>
          <Typography className={classes.first} variant="headline" >
            {firstName}
          </Typography>
          <Typography className={classes.last}>
            {lastName}
          </Typography>
          <Typography component="p">
            {userName}
          </Typography>
          <Typography component="p">
            {address}
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small">More....</Button>
        </CardActions>
      </Card>
    </div>
  );
}

SimpleCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleCard);