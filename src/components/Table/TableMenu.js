import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import Button from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/SearchRounded';
import InputAdornment from '@material-ui/core/InputAdornment';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
  root: {
    paddingRight: theme.spacing.unit,
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
  spacer: {
    display:'flex',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    width: '100%',
    flex: '0 0 auto',
    color: theme.palette.primary.dark,
    fontSize: 20,
  },
  width: {
    width:'100%',
  },
  button: {
    color: '#fff',
    width:'35px',
    height:'35px',
    borderRadius: '5px',
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  icon: {
    fontSize: '18px', 
    color: theme.palette.primary.dark
  },
  searchTextFiled: {
    marginTop: 32,
    width: '500px',
    color: theme.palette.primary.dark,
  },
  textField: {
    width: '100px',
    color: theme.palette.primary.main,
    marginRight: 10
  }
});

let TableToolBar = props => {
  const { 
    numSelected, 
    classes,
    handleFieldChange, 
    searchText, 
    makeSearchRequest, 
    deleteSelected,
    standard, 
    section,
  } = props;

  const standardList = [
    { value: "1", name: "1st" }, 
    { value: "2", name: "2nd" },
    { value: "3", name: "3rd" },
    { value: "4", name: "4th" },
    { value: "5", name: "5th" },
    { value: "6", name: "6th" },
    { value: "7", name: "7th" },
    { value: "8", name: "8th" },
    { value: "9", name: "9th" },
    { value: "10", name: "10th" },
  ];
  const sectionList = [
    { value: "A", name: "A" },
    { value: "B", name: "B" },
    { value: "C", name: "C" },
    { value: "D", name: "D" },
  ]

  return (
    <Toolbar
      className={classNames(classes.root,  {
        [classes.highlight]: numSelected > 0,
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 ? (
          <div className={classes.spacer}>
            <Typography color="inherit" style={{ flexGrow: 1, margin:'auto'}}>
              {numSelected}
              {' '} selected
            </Typography>
            <Tooltip title="Delete">
              <Button aria-label="Delete" className={classes.button} onClick={deleteSelected}>
                <DeleteIcon />
              </Button>
            </Tooltip>
          </div>
        ) : (
            <div className={classes.spacer}>
              <Typography variant="title" id="subheading" style={{ fontSize: '18px', flexGrow: 1, margin: 'auto'}}>
                {/* {title} */}
              </Typography>
              <div>
                <TextField
                  select
                  label={"Class"}
                  className={classes.textField}
                  value={standard}
                  onChange={handleFieldChange("standard")}
                  helperText={"Select Class"}
                  margin="normal"
                >
                  {
                    standardList.map((standard, i) =>
                      <MenuItem key={i} value={standard.value}>
                        {standard.name}
                      </MenuItem>
                    )
                  }
                </TextField>
              </div>
              <div>
                <TextField
                  select
                  label={"Section"}
                  className={classes.textField}
                  value={section}
                  onChange={handleFieldChange("section")}
                  helperText={"Select Section"}
                  margin="normal"
                >
                  {
                    sectionList.map((section, i) =>
                      <MenuItem key={i} value={section.value}>
                        {section.name}
                      </MenuItem>
                    )
                  }
                </TextField>
              </div>
              <form onSubmit={makeSearchRequest}>
                <Typography variant="title" id="subheading">
                  <TextField
                    className={classes.searchTextFiled}
                    placeholder="Search..."
                    value={searchText}
                    onChange={handleFieldChange("searchText")}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <SearchIcon className={classes.icon}/>
                        </InputAdornment>
                      ),
                    }}
                  />
                </Typography>
              </form>
            </div>
          )}
      </div>


    </Toolbar>
  );
};

TableToolBar.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(TableToolBar);