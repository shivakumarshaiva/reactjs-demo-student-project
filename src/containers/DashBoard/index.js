import React from 'react';

import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';
import {createStructuredSelector} from 'reselect';
import {compose} from 'redux';
import {connect} from 'react-redux';

const styles = {
  flex: { display: 'flex' },
  flexColumn: { display: 'flex', flexDirection: 'column' },
  leftCol: { flex: 1, marginRight: '1em' },
  rightCol: { flex: 1, marginLeft: '1em' },
  singleCol: { marginTop: '2em', marginBottom: '2em' },
};

class SimpleLineChart extends React.Component {
  render() {
    return (
      <div style={styles.flex}>
          <div style={styles.leftCol}>
            <div style={styles.flex}>
              <b>No Students Information Found</b>
            </div>
          </div>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
 
});

const withConnect = connect(
  null,
  null,
);

export default compose(
  withConnect,
)(withStyles(styles)(withRouter(SimpleLineChart)));
