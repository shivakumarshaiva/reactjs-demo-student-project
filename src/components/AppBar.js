import React from 'react';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import classNames from 'classnames';
import {withStyles} from '@material-ui/core/styles';
import MainAppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import Profile from './Profile';
import LinearProgress from './LinearProgress';
import {IconButton, Snackbar, SnackbarContent} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import {compose} from 'redux';
import {connect} from 'react-redux';
import { selectFetching, selectIsTokenExpired, selectMessage, selectStatusCode } from '../reducers/genericSelector';
import { isStringIncludes } from '../utils/commonFuctions';
import {createStructuredSelector} from 'reselect';

const drawerWidth = 240;

const styles = theme => ({
  appBar: {
    zIndex: '10000',
    width: '100%',
    top: '0',
    left: 'auto',
    right: '0',
    position: 'fixed',
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 0,
  },
  hide: {
    display: 'none',
  },
  info :{
    backgroundColor: theme.palette.primary.main,
  },
  error:{
    backgroundColor: '#ff5252',
  },
  logo: {
    flexGrow: 1,
    alt: 'No logo found',
    marginLeft:'10px',
    marginTop: '3px',
    width: '50px',
    '@media (max-width: 500px)': {
      width: '100px',
    },
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },

});

class AppBar extends React.Component {

  constructor() {
    super();
    this.state = { 
      isLoading: false,
      snakeOpen: true,
      message:'',
    } 
  }

  handleClose = () => {
    this.setState({ snakeOpen: false,});
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      message: nextProps.message,
    });
    if (nextProps.isLoading) {
      this.setState({
        isLoading: nextProps.isLoading,
        snakeOpen: nextProps.isLoading,
      });
    }
  }

  render() {
    const { theme, classes, className, open, handleDrawerOpen, isLoading, statusCode } = this.props;
    const { snakeOpen, message} = this.state;

    const isTokenexpired = isStringIncludes(message, '401')

    if (isTokenexpired) {
      setTimeout(() => this.props.history.push('/login'), 3000);
    }
    return (
      <div>
        {!isLoading && message &&
          <Snackbar
            open={snakeOpen}
            onClose={this.handleClose}
            autoHideDuration={5000}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
          >
          <SnackbarContent
            className={classNames((statusCode >= 1 && statusCode <= 200) ? classes.info : classes.error, className)}
            aria-describedby="client-snackbar"
            message={
              <span id="client-snackbar" className={classes.message}>
                {isTokenexpired ? 'Your Login Session is Expired, Please Login again!!': message}
              </span>
            }
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                className={classes.close}
                onClick={this.handleClose}
              >
                <CloseIcon className={classes.icon} />
              </IconButton>,
            ]}
          />
          </Snackbar>
        }

        <MainAppBar
          position="absolute"
          className={classNames(classes.appBar, open && classes.appBarShift)}
        >
          <Toolbar disableGutters>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              className={classNames(classes.menuButton, open && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
            <div className={classes.logo}>
              <img src='/app-bar-school-logo.png' alt= "pic" className={classes.logo}/>
            </div>
            <Profile />
          </Toolbar>
          <div style={{ backgroundColor: theme.palette.primary.main, height: '4px' }}>
            {isLoading && <LinearProgress />}
          </div>
        </MainAppBar> 
      </div>
    );
  }
}

AppBar.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  isLoading: PropTypes.bool,
  isTokenExpired: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  isLoading: selectFetching(),
  statusCode:selectStatusCode(),
  message: selectMessage(),
  isTokenExpired: selectIsTokenExpired(),
});

const withConnect = connect(
  mapStateToProps,
  null,
);

export default compose(
  withConnect,
)(withStyles(styles, { withTheme: true })(withRouter(AppBar)));
