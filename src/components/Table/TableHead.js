import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Checkbox from '@material-ui/core/Checkbox';

const styles = theme => ({
  headerText: {
    fontSize: '15px', 
    fontWeight: '300px',
  },
});

class EnhancedTableHead extends React.Component {
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, headerData} = this.props;

    return (
      <TableHead>
        <TableRow>
          {!headerData.isNotDeletable &&  
            <TableCell padding="checkbox">
              <Checkbox
                indeterminate={numSelected > 0 && numSelected < rowCount}
                checked={numSelected > 0 && numSelected === rowCount}
                onChange={onSelectAllClick}
              />
            </TableCell>
          }
          {headerData.columns.map(column => {
            return (
              column.enableSort ?
                <TableCell
                  className={classes.headerText}
                  key={column.label}
                  numeric={column.numeric}
                  sortDirection={orderBy === column.id ? order : false}>
                  <TableSortLabel
                    active={orderBy === column.id}
                    direction={order}
                    onClick={this.createSortHandler(column.id)} >
                    {column.label}
                  </TableSortLabel>
                </TableCell>
                : <TableCell
                  className={classes.headerText}
                  key={column.id}
                  numeric={column.numeric}
                  padding={column.disablePadding ? 'none' : 'default'}>
                  {column.label}
                </TableCell>
            );
          }, this)}
          {headerData.edit && 
          <TableCell className={classes.headerText}>
             Actions
          </TableCell>
          }
        </TableRow>
      </TableHead>
    );
  }
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

export default (withStyles(styles)) (EnhancedTableHead);