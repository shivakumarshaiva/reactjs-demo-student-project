import React from 'react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Tooltip, ListItem, ListItemText, ListItemIcon } from '@material-ui/core';

const styles = theme => ({
  listItem: {
    '&:hover': {
      backgroundColor: theme.palette.primary.light
    }
  }
});

class MyListItem extends React.Component {

  render() {
    const { key, open, classes, onClick, title, icon, iconText } = this.props;
    return (
      <Tooltip disableFocusListener title={open ? "" : title} placement="right">
        <ListItem key={key} button onClick={onClick} className={classes.listItem} >
            {
            icon ? 
              <ListItemIcon>
                {icon}
              </ListItemIcon>
              :
              <div style={{ fontSize: '25px', marginRight: '21px', color: 'rgba(0, 0, 0, 0.54)' }}> 
                <b>{iconText}</b> 
              </div>
            }
          
          <ListItemText primary={title} />
        </ListItem>
      </Tooltip>
    );
  }
}
MyListItem.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withRouter(MyListItem));
