export const CREATE_SUCCESS = 'created successfully';
export const UPDATE_SUCCESS = 'updated successfully';
export const UPLOAD_SUCCESS = 'uploaded successfully';
export const UPLOAD_FAILURE = 'Failure';
export const DELETE_SUCCESS = 'deleted successfully';
export const TRY_AGAIN = 'please try again';

