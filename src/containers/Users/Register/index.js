import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import Form from '../../../components/Form';
import {createStructuredSelector} from 'reselect';
import Alert from '../../../components/Alert';
import {create, RESOURCE_CREATE, RESOURCE_LOOKUP, USERS_RESOURCE,} from '../../../actions';
import sagaHelper from '../../../sagas';
import {default as sagaInjector} from '../../../utils/injectSaga';
import PageHeader from '../../../components/PageHeader';
import ListIcon from '@material-ui/icons/List';
import Card from '@material-ui/core/Card';
import NavigationBar from '../../../components/NavigationBar';
import {emailValidation, phoneDigitLength, phoneNumberFormater as phoneFormatter} from '../../../utils/commonFuctions';
import {CREATE_SUCCESS} from "../../../utils/constonts"
import {selectMessage} from '../../../reducers/genericSelector';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    width: 400,
    margin: '0 auto',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  menu: {
    width: 300,
  },
});

class UserRegister extends React.Component {
  constructor() {
    super();
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      username:'',
      password: '',
      address: '',
      role: null,
      contactNumber: '',
      employeeId:'',
      

      city: '',
      attn:'',
      line1:'',
      line2:'',
      zip:'',
      phone: '',

      file: null,

      isUserRoleValid: null,
      isAddressValid: null,
      isCityValid: null,
      isStreetValid: null,
      isPassword: null,
      isCreated: null,
      isEmailValid: null,
      isUserNameValid: null,
      isFirstNameValid: null,
      isLastNameValid: null,
      showFormInvalid: false,
    };
  }

  componentDidMount(){
    // this.props.makeGetLookupRequest()
  }

  handleFieldChange = (fieldName, isfieldValid = null) => (event) => {
    if (fieldName === "phone" || fieldName === "contactNumber") {
      const onlyNums = event.target && event.target.value.replace(/[^0-9]/g, '');
      if (onlyNums.length < 10) {
        this.setState({ [fieldName]: onlyNums });
      } else if (onlyNums && onlyNums.length === 10) {
        const number = onlyNums && onlyNums.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
        this.setState({ [fieldName]: number });
      }
    } else {
      this.setState({
        [fieldName]: event.target.value,
        ...(isfieldValid && { [isfieldValid]: Boolean(event.target.value) }),
      });
    }
  }

  createdCheck() {
    this.setState({ isCreated: this.props.user ? true : false })
  }

  handleFormSubmit = (event) => {
    event.preventDefault();
    const {
      firstName, lastName, email, password, role, isEmailValid, isFirstNameValid,
      city, attn, line1, line2, zip, file, phone, contactNumber, employeeId, username
    } = this.state;

    if (!firstName) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter User First Name',
      });
      return;
    }
    if (!username) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter User Name',
      });
      return;
    }
    if (!email) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Contact Email',
      });
      return;
    }
    if (email && !emailValidation(email)) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'You Are Antered Invalid Email!!',
      });
      return;
    }
    if (phone && !phoneDigitLength(phone)) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Address\'s  10 Digit Contact Number',
      });
      return;
    }
    if (contactNumber && !phoneDigitLength(contactNumber)) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Personal 10 Digit Contact Number',
      });
      return;
    }
    if (!role) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Select User Role',
      });
      return;
    }
    if (isEmailValid && isFirstNameValid) {
      const payload = {
        firstName,
        lastName,
        username,
        email,
        password,
        contactNumber,
        employeeId,
        city,
        role,
        address: {
          city,
          attn,
          email,
          line1,
          line2,
          zip,
          phone,
        },
        roles:[
          {
            id: role
          }
        ]
      };

      const formData = new FormData();
      { file && formData.append('image', file)}
      formData.append("users", JSON.stringify(payload));
      this.props.makeUserCreationRequest(formData, this.props.history, true);
    } else {
      this.setState({
        showFormInvalid: true,
      });
    }
  }

  snackClose = () => {
    this.setState({ open: false });
  };
  onChange = (e) => {
    this.setState({ file: e.target.files[0] })
  }
  usersListPage = () => {
    this.props.history.push('/users');
  }
  dilogHandle = () => {
    this.setState({
      isDilogOpen: false,
    });
  }
  render() {
    const { firstName, lastName, email, password, role, isEmailValid, isFirstNameValid, username, isUserNameValid,
      isPasswordValid, city, attn, line1, line2, zip, phone, contactNumber, employeeId, isDilogOpen, dilogMessage,
    } = this.state;
    
    const { lookup, selectMessage} = this.props;

    if (selectMessage && selectMessage.indexOf(CREATE_SUCCESS) > -1) {
      this.props.history.push("/users");
    }
   
    const formProps = [
      //user general details
      { type: 'text', label: 'First Name', value: firstName, isValid: isFirstNameValid, fieldName: 'firstName', validFieldName: 'isFirstNameValid', isLeft:true},
      { type: 'text', label: 'Last Name', value: lastName, fieldName: 'lastName', isLeft: true },
      { type: 'text', label: 'User Name', value: username, isValid: isUserNameValid, fieldName: 'username', validFieldName: 'isUserNameValid', isLeft: true },
      { type: 'text', label: 'Password', value: password, isValid: isPasswordValid, fieldName: 'password', validFieldName: 'isPasswordValid', isLeft: true},
      { type: 'text', label: 'Email', value: email, isValid: isEmailValid, fieldName: 'email', validFieldName: 'isEmailValid', isLeft: true},
      { type: 'text', label: 'Contact Number', value: phoneFormatter(contactNumber), fieldName: 'contactNumber', isLeft: true },
      { type: 'text', label: 'Employee Id', value: employeeId, fieldName: 'employeeId', isLeft: true },
      { type: 'select', label: 'User Role', value: role, fieldName: 'role', menuItems: lookup && lookup.userRoles, helperText: "Select User Role", isLeft: true },
      //address details
      { type: 'text', label: 'attn', value: attn, fieldName: 'attn', isLeft: false },
      { type: 'text', label: 'line1', value: line1, fieldName: 'line1', isLeft: false },
      { type: 'text', label: 'line2', value: line2, fieldName: 'line2', isLeft: false },
      { type: 'text', label: 'city', value: city, fieldName: 'city', isLeft: false },
      { type: 'text', label: 'zip', value: zip, fieldName: 'zip', isLeft: false },
      { type: 'text', label: 'Phone', value: phoneFormatter(phone), fieldName: 'phone', isLeft: false },
     
    ];

    const actions = [
      {
        toolTip: "User List",
        icon: <ListIcon />,
        method: this.usersListPage,
      }
    ]
    const alert = <Alert isDilogOpen={isDilogOpen} dilogMessage={dilogMessage} dilogHandle={this.dilogHandle} />
    return (
      <div>
        {isDilogOpen && alert}
        <NavigationBar
          isGoBack={true}
          isRefresh={true}
          actions={actions}
        />
        <Card>
          <PageHeader title={"Create New User"} headerButtons={[]} />
          <Form 
            formProps={formProps} 
            handleFieldChange={this.handleFieldChange} 
            isUpdate={false} 
            handleFormSubmit={this.handleFormSubmit}
            // isFileUpload={true}
            // fileTitle={"Upload Profile Photo"}
            onChange={this.onChange}
            rightTitle={"Address Details"}
            leftTitle={"General Information"}
          />
        </Card>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  makeUserCreationRequest: (payload, history, isMultipart) => dispatch(create(USERS_RESOURCE, payload, history, isMultipart)),
});

const mapStateToProps = createStructuredSelector({
  selectMessage: selectMessage(),
});

UserRegister.propTypes = {
  roles: PropTypes.object,
  lookup: PropTypes.object,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  sagaInjector(sagaHelper(RESOURCE_CREATE)),
  sagaInjector(sagaHelper(RESOURCE_LOOKUP)),
  withConnect,
)(withStyles(styles)(withRouter(UserRegister)));
