import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import {withStyles} from '@material-ui/core/styles';
import axios from "axios";
import jwt from 'jwt-decode';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import LockIcon from '@material-ui/icons/Lock';
import Avatar from '@material-ui/core/Avatar';
import FormHelperText from '@material-ui/core/FormHelperText';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Button from '@material-ui/core/Button';
import LinearProgress from '../../components/LinearProgress';
import Notification from '../../components/Notifications/success';
import config from '../../config';
import classNames from 'classnames';
import CloseIcon from '@material-ui/icons/Close';
import amber from '@material-ui/core/colors/amber';
import {Snackbar, SnackbarContent} from '@material-ui/core';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {selectFetching, selectIsTokenExpired, selectMessage} from '../../reducers/genericSelector';

const styles = theme => ({
  root:{
    backgroundColor: '#2979FF',
  },
  main: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    alignItems: 'center',
    justifyContent: 'flex-start',
    background: 'url(http://www.forallworld.com/data/out/2/IMG_24278.jpg)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  body :{
    margin: 0,
  },
  avatar: {
    backgroundColor: theme.palette.primary.main,
    margin: '0 auto',
    },
  card: {
    minWidth: 300,
    margin: 'auto auto',
  },
  form: {
    padding: '1em 1em 1em 1em',
  },
  button: {
    marginTop: '20px',
    color: '#fff',
    width: '100%',
    borderRadius: '5px',
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  forgotButton: {
    marginTop: '20px',
    color: '#fff',
    width: '50%',
    borderRadius: '5px',
    backgroundColor: theme.palette.primary.light,
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  logo: {
    flexGrow: 1,
    alt: 'No logo found',
    margin: '0 auto',
    width: '100px',
    '@media (max-width: 500px)': {
      width: '100px',
    },
  },
  font: {
    fontSize: '.9rem',
    fontWeight: 300,
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamily,
    flexGrow: 1,
    marginLeft: '111px',
    width: '100%'
  },
  warning: {
    backgroundColor: amber[700],
  },
  message: {
    display: 'flex',
    alignItems: 'center',
    fontFamily: theme.typography.fontFamily,
    fontWeight: 400,
  },
});

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      userName: '',
      password: '',
      token: '',
      role: '',
      showPassword: false,
      isUserNameMissing: null,
      isPasswordMissing: null,
      isLoading: false,
      snakeOpen: true,
      snackBarMessage: null,
      snackBarVariant: null
    };
  }
  handleClose = () => {
    this.setState({ snakeOpen: false, });
  };

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };

  handleFieldChange = (fieldName = null, isfieldValid = null) => (event) => {
    this.setState({
      [fieldName]: event.target.value,
      ...(isfieldValid && { [isfieldValid]: Boolean(event.target.value) }),
    });
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.isLoading) {
      this.setState({
        snakeOpen: nextProps.isLoading,
      });
    }
  }

  submitHandler = (event) =>  {
    event.preventDefault();
    const { history } = this.props;
    const { userName, password,} = this.state
    const body = { userEmail: userName, password: password };
    const { setNotificationMessage } = this;
    const URL = config.BASE_URL +'/login';
    var myThis = this;
    myThis.setState({ isLoading: true });
    axios({
      method: 'post',
      url: URL,
      data: body,
    }).then(function(res) {

      window.localStorage.setItem('token', res.data.token);
      var decoded = jwt(res.data.token);
      window.localStorage.setItem('user', JSON.stringify(decoded));
      setTimeout(() => myThis.setState({ isLoading: false }), history.push('/dashboard'), 3000);

    }).catch(function (error) {

      myThis.setState({ isLoading: false });
      console.log('error while logging in...', error);
      setNotificationMessage('Username password do not match', 'error', false);
      alert("Username password do not match");

    })
  }

  setNotificationMessage = (value, variant, isLoading = false) => {
    if (value) {
      this.setState({ snackBarMessage: value, snackBarVariant: variant, isLoading: false });
    }
    else {
      this.setState({ snackBarMessage: null, snackBarVariant: null, isLoading: false });
    }
  }

  render() {
    const { classes, className, theme, isTokenExpired} = this.props;
    const { 
      userName , 
      password ,
      showPassword,
      isUserNameMissing,
      isPasswordMissing, 
      snakeOpen,
      isLoading,
      snackBarMessage,
      snackBarVariant
    } = this.state
    window.localStorage.removeItem('token');
    window.localStorage.removeItem('user');
    return (
      <div className={classes.main}>
        <Notification
          message={snackBarMessage}
          variant={snackBarVariant}
          handleClose={() => this.setNotificationMessage(null)}
        />
        {
          isTokenExpired &&
          <Snackbar
            open={snakeOpen}
            onClose={this.handleClose}
            autoHideDuration={6000}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
          >
            <SnackbarContent
              className={classNames(classes.info, className)}
              aria-describedby="client-snackbar"
              variant="warning"
              className={classes.root}
              message={
                <span id="client-snackbar" className={classes.message}>
                  {"Your Session is Expired Please Login Again!!"}
                </span>
              }
              action={[
                <IconButton
                  key="close"
                  aria-label="Close"
                  color="inherit"
                  className={classes.close}
                  onClick={this.handleClose}
                >
                  <CloseIcon className={classes.icon} />
                </IconButton>,
              ]}
            />
          </Snackbar>
        }
        <Card className={classes.card} >
          <div style={{ backgroundColor: theme.palette.primary.main, height: '4px' }}>
            {isLoading && <LinearProgress />}
          </div>
          <CardContent style={{ paddingBottom: '0px' }}>
            <Avatar aria-label="Recipe" className={classes.avatar}>
              <LockIcon />
            </Avatar>
            <div className={classes.logo}>
              <img src='/school-logo.png' alt="pic" className={classes.logo} />
            </div>
          </CardContent>
          <form className={classes.form}>              
            <TextField
              style={{margin:'0 auto'}}
              id="name"
              label="User Name"
              value={userName}
              onChange={this.handleFieldChange('userName', 'isUserNameMissing')}
              margin="normal"
              fullWidth
              error={isUserNameMissing === false}
            /> <br/>
            {
              isUserNameMissing === false
              && <FormHelperText className={classes.red} id="name-error-text">
                    Please Enter UserName / E-mail
                </FormHelperText>
            }
            <FormControl style={{width:'100%'}}>
              <InputLabel htmlFor="adornment-password">Password</InputLabel>
                <Input
                  id="adornment-password"
                  type={showPassword ? 'text' : 'password'}
                  value={password}
                  onChange={this.handleFieldChange('password', 'isPasswordMissing')}
                  fullWidth
                  error={isPasswordMissing === false}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="Toggle password visibility"
                        onClick={this.handleClickShowPassword}
                        onMouseDown={this.handleMouseDownPassword}
                      >
                        {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              {
                isPasswordMissing === false
                && <FormHelperText className={classes.red} id="name-error-text">
                  Please Enter password
                </FormHelperText>
              }
              </FormControl>
      
            <Button
              key={1}
              className={classes.button}
              type="submit"
              disabled={isLoading}
              onClick={this.submitHandler}
              >
              Login
            </Button>
          </form>
          <div style={{marginBottom:'14px'}}>
            <Link to="forgot-password" className={classes.font}>
              Forgot Password
          </Link>
          </div>
        </Card>
        
      </div>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
  isTokenExpired: PropTypes.bool,
};
const mapStateToProps = createStructuredSelector({
  isLoading: selectFetching(),
  message: selectMessage(),
  isTokenExpired: selectIsTokenExpired(),
});

const withConnect = connect(
  mapStateToProps,
  null,
);

export default compose(
  withConnect,
)(withStyles(styles, { withTheme: true })(withRouter(Login)));
