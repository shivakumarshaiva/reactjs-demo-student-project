import { REQUEST, createRequestTypes } from './actionUtil';
import { LIST, CREATE, UPDATE, DELETE_MANY, SEARCH } from '../services/api';

export const LOGIN_RESET = 'containers/Users/LOGIN_RESET';

export const USER_LIST = createRequestTypes('USER_LIST');
export const ROLE_LIST = createRequestTypes('ROLE_LIST');
export const USER_CREATE = createRequestTypes('USER_CREATE');
export const USER_COUNT = createRequestTypes('USER_COUNT');
export const UPDATE_USER = createRequestTypes('UPDATE_USER'); 
export const DELETE_ALL = createRequestTypes('DELETE_ALL'); 

export const USER_ENTITY = 'user';
export const ROLE_ENTITY = 'role';

export function createUser(payload) {
  return {
    type: USER_CREATE[REQUEST],
    request: { entity: USER_ENTITY, type: CREATE, payload: payload },
  };
}

export function updateUser(payload) {
  return {
    type: UPDATE_USER[REQUEST],
    request: { entity: USER_ENTITY, type: UPDATE, payload: payload },
  };
}

export function getAllUsers(param) {
  return {
    type: USER_LIST[REQUEST],
    request: { entity: USER_ENTITY, type: LIST, payload: param},
  };
}

export function getRole() {
  return {
    type: ROLE_LIST[REQUEST],
    request: { entity: ROLE_ENTITY , type: LIST },
  };
} 
export function makePageChangeRequest(param) {
  return {
    type: USER_LIST[REQUEST],
    request: { entity: USER_ENTITY, type: LIST, payload: param},
  };
}
export function makeSortingRequest(param) {
  return {
    type: USER_LIST[REQUEST],
    request: { entity: USER_ENTITY, type: LIST, payload: param },
  };
}

export function makeSearchRequest(param) {
  return {
    type: USER_LIST[REQUEST],
    request: { entity: USER_ENTITY, type: SEARCH, payload: param },
  };
}

export function deleteSuccess() {
  return getAllUsers({});
}





