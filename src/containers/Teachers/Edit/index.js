import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import Form from '../Form';
import {
  STUDENTS_RESOURCE,
  getOne,
  RESOURCE_GET_ONE,
  RESOURCE_LOOKUP,
  RESOURCE_UPDATE,
  update,
} from '../../../actions';
import {createStructuredSelector} from 'reselect';
import sagaHelper from '../../../sagas';
import {default as sagaInjector} from '../../../utils/injectSaga';
import { emailValidation, phoneDigitLength, phoneNumberFormater as phoneFormatter, isStringIncludes} from '../../../utils/commonFuctions';
import PageHeader from '../../../components/PageHeader';
import ListIcon from '@material-ui/icons/List';
import Card from '@material-ui/core/Card';
import NavigationBar from '../../../components/NavigationBar';
import Alert from '../../../components/Alert'
import {UPDATE_SUCCESS} from "../../../utils/constonts";
import {selectMessage} from '../../../reducers/genericSelector';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    width: 400,
    margin: '0 auto',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  menu: {
    width: 300,
  },
});

class CustomerUpdate extends React.Component {
  constructor() {
    super();
    this.state = {
      id:'',
      name: '',
      email: '',
      customerCode: '',
      description: '',

      paymentMethods: [],
      shippingMethods: [],
      lookup : {},

      // billingAddress: {
      bcity: '',
      battn: '',
      bline1: '',
      bline2: '',
      bzip: '',
      bphone: '',
      state: '',
      bStateId: '',
      bExtension:'',
      // },
      // shippingAddress: {
      scity: '',
      sattn: '',
      sline1: '',
      sline2: '',
      szip: '',
      sphone: '',
      state: '',
      sStateId : '',
      sExtension:'',
      // },
      states:[],
      sameAsBilling: true,
      taxExempted: false,
      defaultShippingMethod: null,
      defaultPaymentMethod: null,

      isCustomerCodeValid: null,
      showFormInvalid: false,
      
      isDilogOpen: false,
      dilogMessage: '',
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.makeCustomerGetRequest(id);
    this.props.makeGetLookupRequest();
  }
 
  handleFieldChange = (fieldName, isfieldValid = null) => (event) => {
    if (fieldName === "bphone" || fieldName === "sphone") {
      const onlyNums = event.target && event.target.value.replace(/[^0-9]/g, '');
      if (onlyNums.length < 10) {
        this.setState({ [fieldName]: onlyNums });
      } else if (onlyNums && onlyNums.length === 10) {
        const number = onlyNums && onlyNums.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
        this.setState({ [fieldName]: number });
      }
    } else {
      this.setState({
        [fieldName]: event.target.value,
        ...(isfieldValid && { [isfieldValid]: Boolean(event.target.value) }),
      });
    }
  }
  handleTaxChange = (event, checked) => {
    this.setState({ taxExempted: checked });
  };
  handleShippingAddressChange = (event, checked) => {
    this.setState({ sameAsBilling: checked });
  };

  componentWillReceiveProps(nextProps) {

    if (nextProps.customer && nextProps.lookup && nextProps.customer.billingAddress) {
      const {
        id, name, customerCode, description, billingAddress, shippingAddress, sameAsBilling, taxExempted,
        defaultShippingMethod, defaultPaymentMethod, email,
      } = nextProps.customer;
      this.setState({
        id,
        name,
        email,
        customerCode,
        description,
        lookup: nextProps.lookup,

        paymentMethods: nextProps.lookup.paymentMethods,
        shippingMethods: nextProps.lookup.shippingMethods,
        states: nextProps.lookup.taxes,

        // billingAddress: {
        bcity: billingAddress.city,
        battn: billingAddress.attn,
        bline1: billingAddress.line1,
        bline2: billingAddress.line2,
        bzip: billingAddress.zip,
        bphone: phoneFormatter(billingAddress.phone),
        state: billingAddress.state,
        bStateId: billingAddress.stateCode,
        bExtension: billingAddress.extension,
        // },
        // shippingAddress: {
        scity: shippingAddress.city,
        sattn: shippingAddress.attn,
        sline1: shippingAddress.line1,
        sline2: shippingAddress.line2,
        szip: shippingAddress.zip,
        sphone: phoneFormatter(shippingAddress.phone),
        state: shippingAddress.state,
        sStateId: shippingAddress.stateCode,
        sExtension: shippingAddress.extension,
        // },
        sameAsBilling,
        taxExempted,
        defaultShippingMethod,
        defaultPaymentMethod,
      })
    }
  }

  handleFormSubmit = (event) => {
    console.log("customer edit sumitted");
    event.preventDefault();
    const {
      id, name, customerCode, description, taxExempted, defaultShippingMethod,
      defaultPaymentMethod, sameAsBilling, email, bStateId, sStateId,
      bcity, battn, bline1, bline2, bzip, bphone, bExtension,
      scity, sattn, sline1, sline2, szip, sphone, sExtension,
    } = this.state;

    if (!name) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Customer Name',
      })
      return
    }
    if (!email) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Customer Email',
      });
      return;
    }
    if (email && !emailValidation(email)) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'You Are Antered Invalid Email!!',
      });
      return;
    }
    if (bphone && !phoneDigitLength(bphone)) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Billing Address 10 Digit Contact Number',
      });
      return;
    }
    if (sphone && !phoneDigitLength(sphone)) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Shipping Address 10 Digit Contact Number',
      });
      return;
    }

    const payload = {
      id,
      name,
      customerCode,
      description,
      taxExempted,
      defaultShippingMethod,
      defaultPaymentMethod,
      sameAsBilling,
      email,
      billingAddress: {
        city: bcity,
        attn: battn,
        line1: bline1,
        line2: bline2,
        zip: bzip,
        phone: bphone,
        state: bStateId,
        stateCode: bStateId,
        extension: bExtension,
      },
      shippingAddress: {
        city: scity,
        attn: sattn,
        line1: sline1,
        line2: sline2,
        zip: szip,
        phone: sphone,
        state: sStateId,
        stateCode: sStateId,
        extension: sExtension,
      },
    };
    this.props.makeCustomerUpdateRequest(payload, this.props.history, false);
  }

  snackClose = () => {
    this.setState({ open: false });
  };

   customerListPage = () => {
    this.props.history.push('/customers');
  }

  dilogHandle = () => {
    this.setState({
      isDilogOpen: false,
    });
  }
  render() {
    const { name, customerCode, description, defaultShippingMethod, lookup, isDilogOpen, dilogMessage,
      defaultPaymentMethod, email, shippingMethods, paymentMethods, states, 
      bcity, battn, bline1, bline2, bzip, bphone, bStateId, bExtension,
      scity, sattn, sline1, sline2, szip, sphone, sStateId, sExtension,
    } = this.state;
    const { selectMessage } = this.props;
    const isSuccess = isStringIncludes(selectMessage, UPDATE_SUCCESS)

    if (isSuccess) {
      this.props.history.push("/customers");
    }

    let stateList = states && states.map(suggestion => ({
      displayName: suggestion.name,
      name: suggestion.id,
    }))

    const formProps = [
      { type: 'text', label: 'Customer Name', value: name, fieldName: 'name', isTop: true, isEnabled : false },
      { type: 'text', label: 'Customer Code', value: customerCode, fieldName: 'customerCode', isTop: true, isEnabled: true },
      { type: 'text', label: 'Email', value: email, fieldName: 'email', isTop: true, isEnabled: false },
      { type: 'text', label: 'Description', value: description, fieldName: 'description', isTop: true, isEnabled: true },
     
      { type: 'select', label: 'Default Shipping Method', value: defaultShippingMethod, fieldName: 'defaultShippingMethod', menuItems: shippingMethods, helperText: "Default Shipping Method", isTop: true },
      { type: 'select', label: 'Default Payment Method', value: defaultPaymentMethod, fieldName: 'defaultPaymentMethod', menuItems: paymentMethods, helperText: "Default Select Payment Method", isTop: true },
    
    ];

    const billingAddressProps = [
      { type: 'text', label: 'Attn', value: battn, fieldName: 'battn', isLeft: true },
      { type: 'text', label: 'Line1', value: bline1, fieldName: 'bline1', isLeft: true },
      { type: 'text', label: 'Line2', value: bline2, fieldName: 'bline2', isLeft: true },
      { type: 'text', label: 'City', value: bcity, fieldName: 'bcity', isLeft: true },
      { type: 'select', label: 'State', value: bStateId, fieldName: 'bStateId', menuItems: stateList, helperText: "Select State", isLeft: true },
      { type: 'text', label: 'Zip', value: bzip, fieldName: 'bzip', isLeft: true },
      { type: 'text', label: 'Phone', value: bphone, fieldName: 'bphone', isLeft: true },
      { type: 'text', label: 'Extension', value: bExtension, fieldName: 'bExtension', isLeft: true },
     
    ]

    const shippingAddressProps = [
      { type: 'text', label: 'Attn', value: sattn, fieldName: 'sattn', isLeft: false },
      { type: 'text', label: 'Line1', value: sline1, fieldName: 'sline1', isLeft: false },
      { type: 'text', label: 'Line2', value: sline2, fieldName: 'sline2', isLeft: false },
      { type: 'text', label: 'City', value: scity, fieldName: 'scity', isLeft: false },
      { type: 'select', label: 'State', value: sStateId, fieldName: 'sStateId', menuItems: stateList, helperText: "Select State", isLeft: false },
      { type: 'text', label: 'Zip', value: szip, fieldName: 'szip', isLeft: false },
      { type: 'text', label: 'Phone', value: sphone, fieldName: 'sphone', isLeft: false },
      { type: 'text', label: 'Extension', value: sExtension, fieldName: 'sExtension', isLeft: false },
     
    ]

    const actions = [
      {
        toolTip: "Customer List",
        icon: <ListIcon />,
        method: this.customerListPage,
      }
    ]
    const alert = <Alert isDilogOpen={isDilogOpen} dilogMessage={dilogMessage} dilogHandle={this.dilogHandle} />
   
    return (
      <div>
        {isDilogOpen && alert}
        <NavigationBar
          isGoBack={true}
          isRefresh={true}
          actions={actions}
        />
        <Card>
          <PageHeader title={"Update Existing Customer"} headerButtons={[]} />
          <Form
            formProps={formProps}
            billingAddressProps={billingAddressProps}
            shippingAddressProps={shippingAddressProps}
            handleFieldChange={this.handleFieldChange}
            isUpdate={true}
            handleFormSubmit={this.handleFormSubmit}
            title={"Update Existing Customer "}
            topTitle={"Customer Details"}
            onChange={this.onChange}
            leftTitle={"Billing Address"}
            rightTitle={"Shipping Address"}
            handleTaxChange={this.handleTaxChange}
            taxExempted={this.state.taxExempted}
            handleAddressChange={this.handleShippingAddressChange}
            sameAsBilling={this.state.sameAsBilling}
          />
        </Card>
      </div>
    );
  }
}

CustomerUpdate.propTypes = {
  customer: PropTypes.object,
  lookup: PropTypes.object,
};

const mapDispatchToProps = dispatch => ({
  makeCustomerGetRequest: (id) => dispatch(getOne(STUDENTS_RESOURCE, id)),
  makeCustomerUpdateRequest: (payload, history, isMultipart) => dispatch(update(STUDENTS_RESOURCE, payload, history, isMultipart)),
});

const mapStateToProps = createStructuredSelector({
  selectMessage: selectMessage(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  sagaInjector(sagaHelper(RESOURCE_UPDATE)),
  sagaInjector(sagaHelper(RESOURCE_GET_ONE)),
  sagaInjector(sagaHelper(RESOURCE_LOOKUP)),
  withConnect,
)(withStyles(styles)(withRouter(CustomerUpdate)));
