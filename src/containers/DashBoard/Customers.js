import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import SearchIcon from '@material-ui/icons/SearchRounded';
import InputAdornment from '@material-ui/core/InputAdornment';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CustomerAddIcon from '@material-ui/icons/PersonAdd';
import CustomerIcon from '@material-ui/icons/People';
import Divider from '@material-ui/core/Divider';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import TablePagination from '../../components/Table/TablePagination'
import LinearProgress from './../../components/LinearProgress'
import Tooltip from '@material-ui/core/Tooltip';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Alert from '../../components/Alert';
import CardIcon from './CardIcon';

import {
    RESOURCE_LIST,
    STUDENTS_RESOURCE,
    RESOURCE_LOOKUP,
    list,
    search,
} from '../../actions';
import {
    selectCustomers,
    selectCustomersTotal,
    selectIsLoading,
} from '../../reducers/custoners/customerSelector';
import {
    selectLookup,
} from '../../reducers/lookup/lookupSelector';
import sagaHelper from '../../sagas';
import { default as sagaInjector } from '../../utils/injectSaga';
import { createStructuredSelector } from 'reselect';
import { withTheme } from 'styled-components';

const styles = theme => ({
    main: {
        flex: '1',
        marginLeft: '1em',
        marginTop: 20,
    },
    card: {
        padding: '16px 0',
        overflow: 'inherit',
        textAlign: 'right',
    },
    title: {
        padding: '0 16px',
    },
    value: {
        padding: '0 16px',
        minHeight: 48,
    },
    addButton: {
        marginRight: '10px',
        color:'#fff',
        backgroundColor: theme.palette.primary.main,
        '&:hover': {
            backgroundColor: theme.palette.primary.dark,
        },
    },
    avatar: {
        background: theme.palette.background.avatar,
    },
    listItemText: {
        paddingRight: 0,
    },
    // textFiled: {
    //     width: '300px',
    //     minHeight: '40px',
    //     color: theme.palette.primary.dark,
    // },
    formControl: {
        margin: theme.spacing.unit * 3,
    },
    group: {
        margin: `${theme.spacing.unit}px 0`,
    },
    root: {
        display: 'flex',
    },
    button: {
        color: '#fff',
        borderRadius: '5px',
        marginLeft: '15px',
        marginRight:'5px',
        backgroundColor: theme.palette.primary.main,
        '&:hover': {
            backgroundColor: theme.palette.primary.dark,
        },
    },
    radioButton: {
        color: theme.palette.primary.main,
        borderRadius: '5px',
        marginLeft: '5px',
        marginRight: '5px',
        width: '15px',
        height: '15px',
        marginBottom: '10px',
        backgroundColor: '#eceff1',
        '&:hover': {
            backgroundColor: theme.palette.primary.main,
            color: '#fff',
        },
    },
    textField: {
        textAlign:'left',
        width: '250px',
        marginTop: '5px',
        '@media (max-width: 600px)': {
            width: '270px',
            marginLeft: "10%",
        },
    },
});

class Customers extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            order: 'desc',
            orderBy: 'updatedAt',
            selected: [],
            page: 0,
            rowsPerPage: 5,
            searchText: '',
            value: '',
            quotationType:'NONE',
            lookup: {},
            isDilogOpen: false,
            dilogMessage: '',
        };
    }

    componentDidMount() {
        this.getCustomersList(({
            sort: `${this.state.orderBy},${this.state.order}`,
            page: `${this.state.page}`,
            size: `${this.state.rowsPerPage}`
        }));
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.lookup) {
            this.setState({
                lookup: nextProps.lookup,
            });
        }
    }
    getCustomersList = (data = {}) => {
        this.props.getCustomersRequest(data);
    }

    handleChange = (event) => {
        this.setState({
            value: event.target.value,
        });
    }

    handleFieldChange = (event) => {
        this.setState({
            searchText: event.target.value,
        });
    }

    handleSelectChange = (event) => {
        this.setState({
            quotationType: event.target.value,
        });
        console.log("quotationType", this.state.quotationType);
    }

    makeSearchRequest = (event) => {
        event.preventDefault();
        this.props.makeSearchRequest({
            description: this.state.searchText,
            page: `${this.state.page}`,
            size: `${this.state.rowsPerPage}`,
            sort: `${this.state.orderBy},${this.state.order}`,
        });
    }

    handleChangePage = (event, page) => {
        this.setState({ page: page });
        this.setState({ selected: [] })
        if (this.state.searchText) {
            this.props.makeSearchRequest({
                description: this.state.searchText,
                page: `${page}`,
                size: `${this.state.rowsPerPage}`,
                sort: `${this.state.orderBy},${this.state.order}`,
            });
        } else {
            this.props.makePageChangeRequest({
                sort: `${this.state.orderBy},${this.state.order}`,
                page: `${page}`,
                size: `${this.state.rowsPerPage}`
            })
        }
    };

    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: event.target.value });
        if (this.state.searchText) {
            this.props.makeSearchRequest({
                description: this.state.searchText,
                page: `${this.state.page}`,
                size: `${event.target.value}`,
                sort: `${this.state.orderBy},${this.state.order}`,
            });
        } else {
            this.getCustomersList(({
                page: `${this.state.page}`,
                size: `${event.target.value}`,
                sort: `${this.state.orderBy},${this.state.order}`,
            }));
        }
    };

    createQuotation = (event) => {
        if (this.state.quotationType === 'NONE') {
            this.setState({
                isDilogOpen: true,
                dilogMessage: 'Please select any quotation type',
            });
            return;
        } else if (this.state.quotationType) {
            const id = this.state.value;
            const quotationType = this.state.quotationType;
            const url = "/quotations/create/customer/" + id + "/" + quotationType;
            id ? 
            this.props.history.push(url) : 
                this.setState({
                    isDilogOpen: true,
                    dilogMessage: 'Please Select Customer',
                });
            return;
        } 
    }

    createCustomer = () => {
        this.props.history.push('/customers/create')
    }


    dilogHandle = () => {
        this.setState({
            isDilogOpen: false,
        });
    }

    render() {
        const { customers, classes, theme, total, isLoading,} = this.props;
        const { quotationType, lookup, isDilogOpen, dilogMessage} = this.state
        const alert = <Alert isDilogOpen={isDilogOpen} dilogMessage={dilogMessage} dilogHandle={this.dilogHandle} />

        return (
            <div className={classes.main}>
                {isDilogOpen && alert}
                <CardIcon Icon={CustomerIcon} bgColor="#4caf50" />
                <Card className={classes.card}>
                    <Typography className={classes.title} color="textSecondary">
                        <b>{'Customers'}</b>
                    </Typography>
                    <Typography
                        variant="headline"
                        component="h2"
                        className={classes.value}
                    >
                        {total}
                    </Typography>
                    <Divider style={{ height: 2 }} />
                    {isLoading && <LinearProgress />}
                    <div style={{ textAlign: 'right', marginTop: '5px', marginBottom: '15px', height: '50px'}}>
                        <TextField
                            select
                            label={!quotationType ? "Quotation Type" : ""}
                            className={classes.textField}
                            value={quotationType}
                            onChange={this.handleSelectChange}
                            helperText={"Quotation Type"}
                            margin="normal"
                        >
                            {
                                lookup.quotationTypes && lookup.quotationTypes.map((role, i) =>
                                    <MenuItem key={i.name} value={role.name}>
                                        {role.displayName}
                                    </MenuItem>
                                )
                            }
                        </TextField>
                        <Button
                            key={1}
                            style={{ marginTop: 12}}
                            className={classes.button}
                            type="submit"
                            onClick={this.createQuotation}
                        >
                            {"Create Quote"}
                        </Button>
                    </div>
                    <Divider style={{ marginTop: 20, height: 2 }} />
                    <div>
                        <form onSubmit={this.makeSearchRequest} style={{ marginTop: '15px', marginRight: '5px',}}>
                            <Typography variant="title" id="subheading">
                                <TextField
                                    style={{width: '100%'}}
                                    placeholder="Search Customers..."
                                    value={this.searchText}
                                    onChange={this.handleFieldChange}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <SearchIcon className={classes.icon} />
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </Typography>
                        </form>
                    </div>
                    <div style={{ textAlign: 'left' }}>
                        <FormControl style={{ textAlign: 'left' }} component="fieldset" className={classes.formControl}>
                            <RadioGroup
                                name="gender1"
                                className={classes.group}
                                value={this.state.value}
                                onChange={this.handleChange}
                            >
                                {customers && customers.map(record => (
                                    <FormControlLabel style={{ marginTop: '5px', marginBottom: '5px', borderBottomStyle: 'inset' }} value={record.id} control={<Radio className={classes.radioButton} />} label={record.description} />
                                ))}
                            </RadioGroup>
                        </FormControl>
                    </div>
                    <Tooltip title='Create Customer' placement="down">
                        <Button variant="fab" className={classes.addButton} onClick={this.createCustomer}>
                            <CustomerAddIcon />
                        </Button>
                    </Tooltip>
                    <div >
                        <div style={{ width: '60%', marginLeft: '40%'}}>
                            <TablePagination
                                count={total}
                                rowsPerPage={this.state.rowsPerPage}
                                page={this.state.page}
                                handleChangePage={this.handleChangePage}
                                handleChangeRowsPerPage={this.handleChangeRowsPerPage}
                            />
                        </div>
                    </div>
                </Card>
            </div>
        )
    }
}
Customers.propTypes = {
    customers: PropTypes.array,
    total: PropTypes.number,
    lookup: PropTypes.object,
};

const mapDispatchToProps = (dispatch) => ({
    makePageChangeRequest: (param) => dispatch(list(STUDENTS_RESOURCE, param)),
    makeSearchRequest: (param) => dispatch(search(STUDENTS_RESOURCE, param)),
    makeSortingRequest: (param) => dispatch(list(STUDENTS_RESOURCE, param)),
    getCustomersRequest: (param) => dispatch(list(STUDENTS_RESOURCE, param)),
    // makeGetLookupRequest: () => dispatch(getLookup(LOOKUP_RESOURCE))
});

const mapStateToProps = createStructuredSelector({
    customers: selectCustomers(),
    total: selectCustomersTotal(),
    isLoading: selectIsLoading(),
    lookup: selectLookup(),
});

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    sagaInjector(sagaHelper(RESOURCE_LIST)),
     sagaInjector(sagaHelper(RESOURCE_LOOKUP)),
    withConnect,
)((withStyles(styles, { withTheme: true }))(withRouter(Customers)));