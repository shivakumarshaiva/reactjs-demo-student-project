import React from 'react';
import {withRouter} from 'react-router-dom';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {
  Assignment,
  Business,
  ChevronLeft as ChevronLeftIcon,
  ChevronRight as ChevronRightIcon,
  Dashboard,
  FlipToFront,
  InsertLink,
  LocalParking,
  LocalShipping,
  People,
  PermIdentity as VendorIcon,
  Send,
  Payment as Money,
} from '@material-ui/icons';
import {Divider, Drawer, IconButton, List} from '@material-ui/core';
import classNames from 'classnames';
import MyListItem from './MyListItem';
import {intersection} from '../utils/commonFuctions';

var isAccessable = '';
const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'fixed',
    width: '240px',
  },
  drawer: {
    width: '240px',
  },
  drawerMini: {
    '@media (min-width: 481px)': {
      width: '72px',
    },
    width: '56px',
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  }
});

class AppDrawer extends React.Component {
  
  upload = (event) => {
    this.props.history.push('/upload');
  }
  render() {
    const role = (localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')) ) ? JSON.parse(localStorage.getItem('user')).role : '';
    isAccessable = (intersection(role, ['ROLE_ADMIN']).length > 0);
    const { 
      anchor, open, classes, theme, 
      handleDrawerClose, 
      usersPage, 
      studentsPage, 
      teachersPage,
      dashBoard,
     } = this.props;
    return (
      <Drawer
        variant="permanent"
        anchor={anchor}
        open={open}
        className={open ? classes.drawer : classes.drawerMini}
        classes={{
          paper: classNames(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
      >
        <div className={classes.drawerHeader}>
          <span style={{ color: theme.palette.primary.dark, margin: 'auto auto'}}>
            School Admin 1.0
          </span>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <Divider />
        <div className={classes.root}>
          <List component="nav" >
            <MyListItem key={2} open={open} title="Dashboard" onClick={dashBoard} icon={<Dashboard />} />
            <Divider />
           
            <MyListItem key={3} open={open} title="Students" onClick={studentsPage} icon={<Business />} />
            <MyListItem key={4} open={open} title="Teschers" onClick={teachersPage} icon={<Assignment />} />
            <Divider />
            {isAccessable && <MyListItem key={1} open={open} title="Users" onClick={usersPage} icon={<People />} />}
          </List>
        </div>
        <div style={{display:'table-cell', verticalAlign:'middle', textAlign:'center'}}>
          <img style={{width: '15px'}} src='/ganesha.png' alt="" />
        </div>
      </Drawer>
    )
  };
}

AppDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withRouter(AppDrawer));