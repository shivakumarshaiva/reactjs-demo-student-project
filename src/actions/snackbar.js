import { REQUEST, createRequestTypes } from './actionUtil';

export const SNACK_BAR = createRequestTypes('SNACK_BAR');

export function showMessage(open = true, message = '', type = 'INFO', autoHideDuration = 5000) {
  return {
    type: SNACK_BAR[REQUEST],
    request: { 
      open: open, 
      message: message, 
      type: type, 
      autoHideDuration: autoHideDuration 
    },
  };
}

