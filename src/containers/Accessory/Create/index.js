import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import Form from '../../../components/Form';
import {createStructuredSelector} from 'reselect';
import {
    ACCESORIES_RESOURCE,
    create,
    getLookup,
    LOOKUP_RESOURCE,
    RESOURCE_CREATE,
    RESOURCE_LOOKUP,
} from '../../../actions';
import {
  isStringIncludes,
  cleanDecimalNumber,
} from '../../../utils/commonFuctions';
import sagaHelper from '../../../sagas';
import {default as sagaInjector} from '../../../utils/injectSaga';
import PageHeader from '../../../components/PageHeader';
import ListIcon from '@material-ui/icons/List';
import Card from '@material-ui/core/Card';
import NavigationBar from '../../../components/NavigationBar';
import Alert from '../../../components/Alert'
import {CREATE_SUCCESS} from "../../../utils/constonts"
import {selectMessage} from '../../../reducers/genericSelector';
import {selectLookup,} from '../../../reducers/lookup/lookupSelector';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    width: 400,
    margin: '0 auto',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  menu: {
    width: 300,
  },
});

class AccessoryCreation extends React.Component {
  constructor() {
    super();
    this.state = {
      sku: '',
      cost: 0.0,
      unitPrice: 0.0,
      title: '',
      description: '',
      logo: null,
      brand: '',
      vendor: '',
      vendors: [],

      file: null,
      isTitleValid: null,
      showFormInvalid: false,
      isDilogOpen: false,
    };
  }

  componentDidMount() {
    this.props.makeGetLookupRequest();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.lookup) {
      this.setState({
        vendors: nextProps.lookup.vendors,
      });
    }
  }

  handleFieldChange = (fieldName, isfieldValid = null) => (event) => {
    if (fieldName === 'unitPrice' || fieldName === 'cost') {
      this.setState({
        [fieldName]: cleanDecimalNumber(event.target.value),
        ...(isfieldValid && { [isfieldValid]: Boolean(event.target.value) }),
      });
    } else {
      this.setState({
        [fieldName]: event.target.value,
        ...(isfieldValid && { [isfieldValid]: Boolean(event.target.value) }),
      });
    }
  }

  handleFormSubmit = (event) => {
    event.preventDefault();
    const {
      sku, cost, unitPrice, title, description, brand, isTitleValid, file, vendor,
    } = this.state;
    
    if (!sku) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter SKU Number',
      });
      return;
    }

    if (!title) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Title',
      });
      return;
    }

    if (isTitleValid) {
      const payload = {
        sku,
        cost,
        unitPrice,
        title,
        description,
        brand,
        vendor: {
          id: vendor,
        },
      };
      const formData = new FormData();
      { file && formData.append('image', file) }
      formData.append(ACCESORIES_RESOURCE, JSON.stringify(payload));

      this.props.makeAccessoryCreationRequest(formData, this.props.history, true);
    } else {
      this.setState({
        showFormInvalid: true,
      });
    }
  }

  snackClose = () => {
    this.setState({ open: false });
  };
  onChange = (e) => {
    this.setState({ file: e.target.files[0] })
  }
  accessoryListPage = () => {
    this.props.history.push('/accessories')
  }
  dilogHandle = () => {
    this.setState({
      isDilogOpen: false,
    });
  }
  render() {
    const {
      sku, cost, unitPrice, title, description, brand, isTitleValid, isDilogOpen, dilogMessage, vendor, vendors
    } = this.state;
    const { selectMessage } = this.props
    const isSuccess = isStringIncludes(selectMessage, CREATE_SUCCESS)

    if (isSuccess) {
      this.props.history.push("/accessories");
    }

    let vendorList = vendors && vendors.map(suggestion => ({
      displayName: suggestion.name,
      name: suggestion.id,
    }))

    const formProps = [
      //accessory details
      { type: 'text', label: 'Title', value: title, isValid: isTitleValid, fieldName: 'title', validFieldName: 'isTitleValid', isLeft: true },
      { type: 'text', label: 'SKU', value: sku, fieldName: 'sku', isLeft: true },
      { type: 'text', label: 'Unit Price', value: unitPrice, fieldName: 'unitPrice', isLeft: true },
      { type: 'text', label: 'Cost', value: cost, fieldName: 'cost', isLeft: true },
      { type: 'text', label: 'Description', value: description, fieldName: 'description', isLeft: true },
      { type: 'text', label: 'Brand', value: brand, fieldName: 'brand',  isLeft: true },
      { type: 'select', label: 'Vendor', value: vendor, fieldName: 'vendor', menuItems: vendorList, helperText: "Select Vendor", isLeft: true },
    ];

    const actions = [
      {
        toolTip: "Accessory List",
        icon: <ListIcon />,
        method: this.accessoryListPage,
      }
    ]
    const alert = <Alert isDilogOpen={isDilogOpen} dilogMessage={dilogMessage} dilogHandle={this.dilogHandle} />
    
    return (
      <div>
        {isDilogOpen && alert}
        < NavigationBar
          isGoBack={true}
          isRefresh={true}
          actions={actions}
        />
        <Card>
          <PageHeader title={"Create New Accessory"} headerButtons={[]} />
          <Form
            formProps={formProps}
            handleFieldChange={this.handleFieldChange}
            isUpdate={false}
            handleFormSubmit={this.handleFormSubmit}
            // isFileUpload={true}
            // fileTitle={"Upload Accessory image"}
            onChange={this.onChange}
            leftTitle={"Accessory Details"}
          />
        </Card>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  makeGetLookupRequest: () => dispatch(getLookup(LOOKUP_RESOURCE)),
  makeAccessoryCreationRequest: (payload, history, isMultipart) => dispatch(create(ACCESORIES_RESOURCE, payload, history, isMultipart)),
});


const mapStateToProps = createStructuredSelector({
  selectMessage: selectMessage(),
  lookup: selectLookup(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  sagaInjector(sagaHelper(RESOURCE_CREATE)),
  sagaInjector(sagaHelper(RESOURCE_LOOKUP)),
  withConnect,
)(withStyles(styles)(withRouter(AccessoryCreation)));
