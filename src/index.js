import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from 'react-router-dom'
import App from "../src/containers/App";
import registerServiceWorker from "./registerServiceWorker";
import { Provider } from "react-redux";
import configureStore from './store/configureStore';
import "babel-polyfill";

const store = configureStore({});

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
     <App store={store}/>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);

registerServiceWorker();
