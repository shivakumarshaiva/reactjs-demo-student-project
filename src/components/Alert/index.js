import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class Alert extends React.Component {
  constructor() {
    super();
    this.state = {
    };
  }

  render() {
    const { isDilogOpen, dilogHandle, dilogMessage, dilogTitle } = this.props;

    return (
      <div>
        <Dialog
          open={isDilogOpen}
          TransitionComponent={Transition}
          keepMounted
          onClose={dilogHandle}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">
            {dilogTitle}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              {dilogMessage && dilogMessage}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={dilogHandle} color="primary">
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default Alert;
