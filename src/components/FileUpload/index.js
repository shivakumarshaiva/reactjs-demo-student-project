import React, {Component} from 'react';
import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';

registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview);

class FileUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {
        files: []
    };
  }

  handleInit() {
      console.log('FilePond instance has initialised', this.pond);
  }

  render() {
      const { classes, handleFileChange, files, isMultipleFiles} = this.props;
      return (
          <div style={{ marginBottom: 30}}>
              <FilePond ref={ref => this.pond = ref}
                files={files}
                allowMultiple={isMultipleFiles}
                server={{
                    process: (fieldName, file, metadata, load) => {
                        setTimeout(() => {
                            load(Date.now())
                        }, 500);
                    },
                    load: (source, load) => {
                        fetch({source}).then(res => res.blob()).then(load);
                    }
                }}
                oninit={() => this.handleInit() }
                onupdatefiles={fileItems => { handleFileChange(fileItems)}}>
              </FilePond>
          </div>
      );
  }
}

export default FileUpload;