import { combineReducers } from 'redux';
import genericReducer from './genericReducer';

const rootReducer = combineReducers({
  root: genericReducer,
})

export default rootReducer