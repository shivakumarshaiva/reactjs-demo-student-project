import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import Form from '../Form';
import {
  STUDENTS_RESOURCE,
  getOne,
  RESOURCE_GET_ONE,
  RESOURCE_LOOKUP,
  RESOURCE_CREATE,
  create,
} from '../../../actions';
import { createStructuredSelector } from 'reselect';
import sagaHelper from '../../../sagas';
import { default as sagaInjector } from '../../../utils/injectSaga';
import { emailValidation, phoneDigitLength, phoneNumberFormater as phoneFormatter, isStringIncludes } from '../../../utils/commonFuctions';
import PageHeader from '../../../components/PageHeader';
import ListIcon from '@material-ui/icons/List';
import Card from '@material-ui/core/Card';
import NavigationBar from '../../../components/NavigationBar';
import Alert from '../../../components/Alert'
import { UPDATE_SUCCESS } from "../../../utils/constonts";
import { selectMessage } from '../../../reducers/genericSelector';
import { selectStudent } from '../../../reducers/students/studentsSelector';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    width: 400,
    margin: '0 auto',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  menu: {
    width: 300,
  },
});

class CustomerUpdate extends React.Component {
  constructor() {
    super();
    this.state = {
      id: '',
      firstName: '',
      lastName: "",
      class: 10,
      section: "A",
      admissionNumber: '',
      rollNumber: "",
      dateOfBirth: '',
      gender: "",
      religion: "",
      caste: "",
      aadharNumber: "",
      dateOfAdmission: '',
      // parentDetails: {
      fatherName: " ",
      motherName: "",
      phoneNumber: '',
      alternateNumber: '',
      fatherOccupation: "",
      motherOccupation: "",
      fatherAadharNo: "",
      motherAadharNo: "",
      // },
      // address: {
      doorNum: "",
      street: "",
      locality: "",
      city: "",
      state: "",
      pincode: '',
      contact: '',
      // },
      isDilogOpen: false,
      dilogMessage: '',
    };
  }

  // componentDidMount() {
  //   const id = this.props.match.params.id;
  //   this.props.makeStudentGetRequest(id);
  // }

  handleFieldChange = (fieldName, isfieldValid = null) => (event) => {
    this.setState({
      [fieldName]: event.target.value,
      ...(isfieldValid && { [isfieldValid]: Boolean(event.target.value) }),
    });
  }

  // componentWillReceiveProps(nextProps) {

  //   if (nextProps.student) {
  //     const {
  //       _id,
  //       firstName,
  //       lastName,
  //       standard,
  //       section,
  //       admissionNumber,
  //       rollNumber,
  //       dateOfBirth,
  //       gender,
  //       religion,
  //       caste,
  //       aadharNumber,
  //       dateOfAdmission,
  //       parentDetails,
  //       address,
  //     } = nextProps.student;
  //     this.setState({
  //       id: _id,
  //       firstName: firstName,
  //       lastName: lastName,
  //       class: standard,
  //       section: section,
  //       admissionNumber: admissionNumber,
  //       rollNumber: rollNumber,
  //       dateOfBirth: dateOfBirth && dateOfBirth.split('T')[0],
  //       gender: gender,
  //       religion: religion,
  //       caste: caste,
  //       aadharNumber: aadharNumber,
  //       // dateOfAdmission: dateOfAdmission && dateOfAdmission.split('T')[0] && dateOfAdmission.split('T')[0].split('-')[2] + '-' + dateOfAdmission.split('T')[0].split('-')[0] + '-' + dateOfAdmission.split('T')[0].split('-')[1],
  //       dateOfAdmission: dateOfAdmission && dateOfAdmission.split('T')[0],
  //       // parentDetails: {
  //       fatherName: parentDetails && parentDetails.fatherName && parentDetails.fatherName,
  //       motherName: parentDetails && parentDetails.motherName && parentDetails.motherName,
  //       phoneNumber: parentDetails && parentDetails.phoneNumber && parentDetails.phoneNumber,
  //       alternateNumber: parentDetails && parentDetails.alternateNumber && parentDetails.alternateNumber,
  //       fatherOccupation: parentDetails && parentDetails.fatherOccupation && parentDetails.fatherOccupation,
  //       motherOccupation: parentDetails && parentDetails.motherOccupation && parentDetails.motherOccupation,
  //       fatherAadharNo: parentDetails && parentDetails.fatherAadharNo && parentDetails.fatherAadharNo,
  //       motherAadharNo: parentDetails && parentDetails.motherAadharNo && parentDetails.motherAadharNo,
  //       // },
  //       // address: {
  //       doorNum: address && address.doorNum,
  //       street: address && address.street,
  //       locality: address && address.locality,
  //       city: address && address.city,
  //       state: address && address.state,
  //       pincode: address && address.pincode,
  //       contact: address && address.contact
  //       // },
  //     })
  //   }
  // }

  handleFormSubmit = (event) => {
    console.log("student edit sumitted");
    event.preventDefault();
    const {
      id,
      firstName,
      lastName,
      standard,
      section,
      admissionNumber,
      rollNumber,
      dateOfBirth,
      gender,
      religion,
      caste,
      aadharNumber,
      dateOfAdmission,

      fatherName,
      motherName,
      phoneNumber,
      alternateNumber,
      fatherOccupation,
      motherOccupation,
      fatherAadharNo,
      motherAadharNo,

      doorNum,
      street,
      locality,
      city,
      state,
      pincode,
      contact
    } = this.state;

    if (!firstName) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Student Name',
      })
      return
    }
    if (!aadharNumber) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Student Adhar Number',
      });
      return;
    }
    if (!dateOfAdmission) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Aate Of Admission',
      });
      return;
    }
    if (!rollNumber) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Roll Number',
      });
      return;
    }

    const payload = {
      _id: id,
      firstName: firstName,
      lastName: lastName,
      class: standard,
      section: section,
      admissionNumber: admissionNumber,
      rollNumber: rollNumber,
      dateOfBirth: dateOfBirth,
      gender: gender,
      religion: religion,
      caste: caste,
      aadharNumber: aadharNumber,
      dateOfAdmission: dateOfAdmission,
      parentDetails: {
        fatherName: fatherName,
        motherName: motherName,
        phoneNumber: phoneNumber,
        alternateNumber: alternateNumber,
        fatherOccupation: fatherOccupation,
        motherOccupation: motherOccupation,
        fatherAadharNo: fatherAadharNo,
        motherAadharNo: motherAadharNo,
      },
      address: {
        doorNum: doorNum,
        street: street,
        locality: locality,
        city: city,
        state: state,
        pincode: pincode,
        contact: contact
      },
    };
    this.props.makeStudentUpdateRequest(payload, false);
  }

  snackClose = () => {
    this.setState({ open: false });
  };

  studentListPage = () => {
    this.props.history.push('/students');
  }

  dilogHandle = () => {
    this.setState({
      isDilogOpen: false,
    });
  }

  handleDateFieldChange = (event) => {
    this.setState({
      shippingDate: event.target.value
    })
  }


  render() {
    const {
      isDilogOpen,
      dilogMessage,
      firstName,
      lastName,
      standard,
      section,
      admissionNumber,
      rollNumber,
      dateOfBirth,
      gender,
      religion,
      caste,
      aadharNumber,
      dateOfAdmission,

      fatherName,
      motherName,
      phoneNumber,
      alternateNumber,
      fatherOccupation,
      motherOccupation,
      fatherAadharNo,
      motherAadharNo,

      doorNum,
      street,
      locality,
      city,
      state,
      pincode,
      contact
    } = this.state;
    const { selectMessage } = this.props;
    const isSuccess = isStringIncludes(selectMessage, UPDATE_SUCCESS)

    if (isSuccess) {
      this.props.history.push("/students");
    }

    // const date = dateOfBirth && dateOfBirth.split('T')[0];

    // var format = date && date.split('-')

    // console.log('dateOfBirth.split :', dateOfBirth && dateOfBirth.split('T')[0] && dateOfBirth.split('T')[0].split('-')[2] + '-' + dateOfBirth.split('T')[0].split('-')[1] + '-' + dateOfBirth.split('T')[0].split('-')[0]);
    // let stateList = states && states.map(suggestion => ({
    //   displayName: suggestion.name,
    //   name: suggestion.id,
    // }))

    const standardList = [
      { value: "1", name: "1st" },
      { value: "2", name: "2nd" },
      { value: "3", name: "3rd" },
      { value: "4", name: "4th" },
      { value: "5", name: "5th" },
      { value: "6", name: "6th" },
      { value: "7", name: "7th" },
      { value: "8", name: "8th" },
      { value: "9", name: "9th" },
      { value: "10", name: "10th" },
    ];
    const sectionList = [
      { value: "A", name: "A" },
      { value: "B", name: "B" },
      { value: "C", name: "C" },
      { value: "D", name: "D" },
    ]
    const genderList = [
      { value: "Male", name: "Male" },
      { value: "F-Male", name: "F-Male" },
    ]

    const formProps = [

      { type: 'text', label: 'First Name', value: firstName, fieldName: 'firstName', isTop: true, isEnabled: true, isLeft: false },
      { type: 'text', label: 'Last Name', value: lastName, fieldName: 'lastName', isTop: true, isEnabled: true, isLeft: false },
      { type: 'select', label: 'Class', value: standard, fieldName: 'standard', menuItems: standardList, helperText: "Select Class", isLeft: true },
      { type: 'select', label: 'Section', value: section, fieldName: 'section', menuItems: sectionList, helperText: "Select Section", isLeft: true },
      { type: 'text', label: 'Admission Number', value: admissionNumber, fieldName: 'admissionNumber', isTop: true, isEnabled: false, isLeft: false },
      { type: 'text', label: 'Roll Number', value: rollNumber, fieldName: 'rollNumber', isTop: true, isEnabled: true, isLeft: false },
      { type: 'text', label: 'AdharNumber', value: aadharNumber, fieldName: 'aadharNumber', isTop: true, isEnabled: true, isLeft: false },
      { type: 'date', label: 'Date Of Birth', value: dateOfBirth, fieldName: 'dateOfBirth', isTop: true, isEnabled: true, isLeft: false },
      { type: 'date', label: 'Date Of Admission', value: dateOfAdmission, fieldName: 'dateOfAdmission', isTop: true, isEnabled: true, isLeft: false },
      { type: 'select', label: 'Gender', value: gender, fieldName: 'gender', menuItems: genderList, helperText: "Select Gender", isLeft: true },
      { type: 'text', label: 'Religion', value: religion, fieldName: 'religion', isTop: true, isEnabled: true, isLeft: true },
      { type: 'text', label: 'Caste', value: caste, fieldName: 'caste', isTop: true, isEnabled: true, isLeft: true },
    ];

    const parentDetails = [
      { type: 'text', label: 'Father Name', value: fatherName, fieldName: 'fatherName', isLeft: true },
      { type: 'text', label: 'Mother Name', value: motherName, fieldName: 'motherName', isLeft: true },
      { type: 'text', label: 'Phone Number', value: phoneNumber, fieldName: 'phoneNumber', isLeft: true },
      { type: 'text', label: 'Alternate Number', value: alternateNumber, fieldName: 'alternateNumber', isLeft: true },
      { type: 'text', label: 'Father Occupation', value: fatherOccupation, fieldName: 'fatherOccupation', isLeft: true },
      { type: 'text', label: 'Mother Occupation', value: motherOccupation, fieldName: 'motherOccupation', isLeft: true },
      { type: 'text', label: 'Father Adhar Number', value: fatherAadharNo, fieldName: 'fatherAadharNo', isLeft: true },
      { type: 'text', label: 'Mother Adhar Number', value: motherAadharNo, fieldName: 'motherAadharNo', isLeft: true },
    ]

    const address = [
      { type: 'text', label: 'Door#', value: doorNum, fieldName: 'doorNum', isLeft: false },
      { type: 'text', label: 'Street', value: street, fieldName: 'street', isLeft: false },
      { type: 'text', label: 'Locality', value: locality, fieldName: 'locality', isLeft: false },
      { type: 'text', label: 'City', value: city, fieldName: 'city', isLeft: false },
      { type: 'text', label: 'State', value: state, fieldName: 'state', isLeft: false },
      { type: 'text', label: 'Zip', value: pincode, fieldName: 'pincode', isLeft: false },
      { type: 'text', label: 'Phone', value: contact, fieldName: 'contact', isLeft: false },
    ]

    const actions = [
      {
        toolTip: "Student List",
        icon: <ListIcon />,
        method: this.studentListPage,
      }
    ]
    const alert = <Alert isDilogOpen={isDilogOpen} dilogMessage={dilogMessage} dilogHandle={this.dilogHandle} />

    return (
      <div>
        {isDilogOpen && alert}
        <NavigationBar
          isGoBack={true}
          isRefresh={true}
          actions={actions}
        />
        <Card>
          <PageHeader title={"Create Student"} headerButtons={[]} />
          <Form
            formProps={formProps}
            parentDetails={parentDetails}
            address={address}
            handleFieldChange={this.handleFieldChange}
            isUpdate={true}
            handleFormSubmit={this.handleFormSubmit}
            title={"Create Student "}
            topTitle={"Student Details"}
            onChange={this.onChange}
            leftTitle={"Parents Info"}
            rightTitle={"Address"}
            handleTaxChange={this.handleTaxChange}
            taxExempted={this.state.taxExempted}
            handleAddressChange={this.handleShippingAddressChange}
            sameAsBilling={this.state.sameAsBilling}
          />
        </Card>
      </div>
    );
  }
}

CustomerUpdate.propTypes = {
  customer: PropTypes.object,
};

const mapDispatchToProps = dispatch => ({
  // makeStudentGetRequest: (id) => dispatch(getOne(STUDENTS_RESOURCE, id)),
  makeStudentUpdateRequest: (payload, isMultipart) => dispatch(create(STUDENTS_RESOURCE, payload, isMultipart)),
});

const mapStateToProps = createStructuredSelector({
  selectMessage: selectMessage(),
  // student: selectStudent(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  sagaInjector(sagaHelper(RESOURCE_CREATE)),
  // sagaInjector(sagaHelper(RESOURCE_GET_ONE)),
  withConnect,
)(withStyles(styles)(withRouter(CustomerUpdate)));
