import { REQUEST, createRequestTypes } from './actionUtil';
import { 
  GET_ONE, LIST, CREATE, UPDATE, DELETE, SEARCH, UPLOAD
} from '../services/api';

export const RESOURCE_GET_ONE = createRequestTypes('RESOURCE_GET_ONE');
export const RESOURCE_CREATE = createRequestTypes('RESOURCE_CREATE');
export const RESOURCE_UPLOAD = createRequestTypes('RESOURCE_UPLOAD');
export const RESOURCE_LIST = createRequestTypes('RESOURCE_LIST');
export const RESOURCE_DELETE = createRequestTypes('RESOURCE_DELETE');
export const RESOURCE_SEARCH = createRequestTypes('RESOURCE_SEARCH');
export const RESOURCE_UPDATE = createRequestTypes('RESOURCE_UPDATE');
export const RESOURCE_LOOKUP = createRequestTypes('RESOURCE_LOOKUP');

export const USER_RESOURCE_LIST = createRequestTypes('USER_RESOURCE_LIST');
export const STUDENT_RESOURCE_LIST = createRequestTypes('STUDENT_RESOURCE_LIST');
export const STUDENT_RESOURCE_SEARCH = createRequestTypes('STUDENT_RESOURCE_SEARCH');

export const USERS_RESOURCE = 'users'; // All CRUD operations
export const TEACHER_RESOURCE = 'teachers'; // All CRUD operations
export const STUDENTS_RESOURCE = 'students'; 

export function create(resource, payload, isMultipart) { 
  return {
    type: RESOURCE_CREATE[REQUEST],
    isMultipart,
    resource,
    request: { type: CREATE, payload: payload },
  };
}

export function update(resource, payload, isMultipart) {
  return {
    type: RESOURCE_UPDATE[REQUEST],
    isMultipart,
    resource,
    request: { type: UPDATE, payload: payload },
  };
}

export function upload(resource, payload) {
  return {
    type: RESOURCE_UPDATE[REQUEST],
    resource,
    request: { type: UPLOAD, payload: payload },
  };
}

export function deleteResource(resource, param) {
  return {
    type: RESOURCE_DELETE[REQUEST],
    resource,
    request: { type: DELETE, payload: param },
  };
}

export function getOne(resource, param) {
  return {
    type: RESOURCE_GET_ONE[REQUEST],
    resource,
    request: { type: GET_ONE, payload: param },
  };
}

export function list(resource, param) {
  return {
    type: RESOURCE_LIST[REQUEST],
    resource,
    request: { type: LIST, payload: param },
  };
}
export function studenntlist(resource, param) {
  return {
    type: STUDENT_RESOURCE_LIST[REQUEST],
    resource,
    request: { type: LIST, payload: param },
  };
}

export function search(resource, param) {
  return {
    type: RESOURCE_LIST[REQUEST],
    resource,
    request: { type: SEARCH, payload: param },
  };
}

export function studenntSearch(resource, param) {
  return {
    type: STUDENT_RESOURCE_SEARCH[REQUEST],
    resource,
    request: { type: SEARCH, payload: param },
  };
}

export function deleteSuccess(resource, param) {
  return list(resource, param);
}




