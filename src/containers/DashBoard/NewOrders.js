import React from 'react';
import CardIcon from './CardIcon';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';

import { withStyles } from '@material-ui/core/styles';

const styles = {
  main: {
      flex: '1',
      marginLeft: '1em',
      marginTop: 20,
  },
  card: {
      overflow: 'inherit',
      textAlign: 'right',
      padding: 16,
      minHeight: 52,
  },
};

class NewOrders extends React.Component {
  render() {
    const { open, classes, onClick, title, icon, value } = this.props;
    return (
      <div className={classes.main}>
          <CardIcon Icon={ShoppingCartIcon} bgColor="#ff9800" />
          <Card className={classes.card}>
              <Typography className={classes.title} color="textSecondary">
                  New Orders
              </Typography>
              <Typography variant="headline" component="h2">
                  {value}
              </Typography>
          </Card>
      </div>
    );
  }
}

export default withStyles(styles)(NewOrders);