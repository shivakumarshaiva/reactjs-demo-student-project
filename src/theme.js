import { createMuiTheme } from '@material-ui/core/styles';

export default createMuiTheme({
  palette: {
    primary: {
      light: '#bb6bc9',
      main: '#ab47bc',
      dark: '#773183',
    },
    secondary: {
      light: '#bb6bc9',
      main: '#ab47bc',
      dark: '#773183',
    },
  },

  overrides: {
    MuiButton: { // Name of the component ⚛️ / style sheet
      root: { // Name of the rule
        color: '#155fa0', 
      },
    },
    MuiTab: { // Name of the component ⚛️ / style sheet
      root: { // Name of the rule
        color: '#000',
      },
    },
  },
});