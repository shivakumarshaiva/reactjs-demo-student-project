import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from './TableHead';
import TableMenu from './TableMenu';
import TableBody from './TableBody';
import TablePagination from './TablePagination';

const styles = theme => ({
  root: {
    width: '100%',
  },
  table: {
    minWidth: '100%',
    '@media (max-width: 480px)': {
      display: 'none',
    },
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  spacer: {
    flex: '1 1 100%',
  },
});

class EnhancedTable extends React.Component {

  render() {
    const {
      classes,
      total,
      tableData,
      handleChangePage,
      handleChangeRowsPerPage,
      rowsPerPage,
      page,
      handleClick,
      isSelected,
      handleSelectAllClick,
      handleRequestSort,
      selected,
      order,
      orderBy,
      handleFieldChange,
      makeSearchRequest,
      searchText,
      deleteSelected,
      isLoading,
      pageHeader,
      actions,
      standard,
      section
    } = this.props;
    return (
      <div>
        {pageHeader}
        <TableMenu
        standard={standard}
        section={section}
        numSelected={selected && selected.length}
        searchText={searchText}
        handleFieldChange={handleFieldChange}
        makeSearchRequest={makeSearchRequest}
        deleteSelected={deleteSelected}
        />
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <TableHead
              headerData={tableData}
              numSelected={selected && selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={tableData && tableData.rowData && tableData.rowData.length}
            />
            <TableBody
              actions={actions}
              handleClick={handleClick}
              isSelected={isSelected}
              tableData={tableData}
            />
          </Table>
        </div>
        <TablePagination
          count={total}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangePage={handleChangePage}
          handleChangeRowsPerPage={handleChangeRowsPerPage}
        />
        </div>
    );
  }
}

EnhancedTable.propTypes = {
  users: PropTypes.array,
  total: PropTypes.number
};

export default (withStyles(styles)(withRouter(EnhancedTable)));
