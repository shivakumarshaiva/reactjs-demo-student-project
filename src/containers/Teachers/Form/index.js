import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import classNames from 'classnames';
import Grid from '@material-ui/core/Grid';
import Switch from '@material-ui/core/Switch';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: '0px',
  },
  paperHead: {
    padding: theme.spacing.unit * 2,
    textAlign: 'left',
    color: theme.palette.text.secondary,
    display: 'flex',
    boxShadow: 'none',
  },
  paperBody: {
    color: theme.palette.text.secondary,
    boxShadow: 'none',
  },
  container: {
    display: 'flex',
    marginLeft: '20px',
    width: '50%',
    '@media (max-width: 600px)': {
      width: '100%',
      flexWrap: 'wrap',
      marginRight: '20px',
      display: 'block',
    },

  },
  header : {
    display: 'flex',
    marginLeft: '20px',
    width: '50%',
    '@media (max-width: 600px)': {
      width: '100%',
      flexWrap: 'wrap',
      marginRight: '20px',
      display: 'block',
    },
  },
  textField: {
    marginTop: '4px',
    marginBottom: '2px',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    '@media (max-width: 600px)': {
      width: '270px',
    },
    width: 500,
  },

  font: {
    fontSize: '1.5rem',
    fontWeight: 400,
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamily,
    flexGrow: 1,
  },

  title: {
    fontSize: '1rem',
    fontWeight: 400,
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamily,
    flexGrow: 1,
    marginTop: '10px',
  },

  photoFont: {
    fontSize: '.8rem',
    fontWeight: 400,
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamily,
    flexGrow: 1,
    marginTop: '10px',
  },
  errorFont: {
    fontSize: '1',
    fontWeight: 400,
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamily,
    flexGrow: 1,
  },
  button: {
    margin: '15px 0',
    color: '#fff',
    borderRadius: '5px',
    marginLeft: '25px',
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
  },
});

const Form = ({
  classes, formProps, handleFieldChange, handleFormSubmit, isUpdate,leftTitle, 
  rightTitle, topTitle, billingAddressProps, shippingAddressProps, handleTaxChange, 
  taxExempted, handleAddressChange, sameAsBilling,
}) => (
    <div className={classes.root}>
      <Grid  spacing={24} >
        <Grid item xs={12} style={{ padding: '0px' }}>
          <Card className={classes.paperHead}>
            <form className={classes.header} noValidate autoComplete="off">
              <div >
                <Typography variant="title" className={classes.title}>
                  {topTitle}
                </Typography>
                {formProps &&
                  formProps
                    .map((formField, i) => {
                      return (formField &&
                        (formField.type === 'text' ?
                          <div>
                            <TextField
                              type={formField.type}
                              label={formField.label}
                              value={formField.value}
                              disabled={formField.isEnabled}
                              onChange={handleFieldChange(formField.fieldName, formField.validFieldName)}
                              className={classes.textField}
                              fullWidth
                              margin="normal"
                              error={formField.isValid === false}
                            />
                            {
                              formField.isValid === false &&
                              <FormHelperText className={classes.red} id="name-error-text" >
                                <span className={classes.errorFont}>
                                  Required Field
                                </span>
                              </FormHelperText>
                            }
                          </div>
                          :
                          <TextField
                            select
                            label={!formField.value && formField.helperText}
                            className={classes.textField}
                            value={formField.value}
                            onChange={handleFieldChange(formField.fieldName)}
                            helperText={formField.helperText}
                            margin="normal"
                          >
                            {
                              formField.menuItems && formField.menuItems.map((role, i) =>
                                <MenuItem key={i.name} value={role.name}>
                                  {role.displayName}
                                </MenuItem>
                              )
                            }
                          </TextField>
                        )
                      );
                    })
                }
                <div>
                  <FormControlLabel
                    label={taxExempted ? 'Tax Exempted' : 'Tax Not Exempted'}
                    control={
                      <Switch checked={taxExempted} onChange={handleTaxChange} aria-label="TaxSwitch" />
                    }
                  />
                </div>
            </div>
          </form>
          </Card>
        </Grid>

        <Grid item xs={12} style={{ padding: '0px' }}>
          <Card className={classes.paperBody}>
            <form className={classes.container} noValidate autoComplete="off">
              <div >
                <div>
                  <Typography variant="title" className={classes.title}>
                    {leftTitle}
                  </Typography>
                  {billingAddressProps &&
                    billingAddressProps
                      .map((formField, i) => {
                        return (formField.isLeft &&
                          (formField.type === 'text' ?
                            <div>
                              <TextField
                                type={formField.type}
                                label={formField.label}
                                value={formField.value}
                                onChange={handleFieldChange(formField.fieldName, formField.validFieldName)}
                                className={classes.textField}
                                fullWidth
                                margin="normal"
                                error={formField.isValid === false}
                              />
                              {
                                formField.isValid === false &&
                                <FormHelperText className={classes.red} id="name-error-text" >
                                  <span className={classes.errorFont}>
                                    Required Field
                                      </span>
                                </FormHelperText>
                              }
                            </div>
                            :
                            <TextField
                              select
                              label={!formField.value && formField.helperText}
                              className={classes.textField}
                              value={formField.value}
                              onChange={handleFieldChange(formField.fieldName)}
                              helperText={formField.helperText}
                              margin="normal"
                            >
                              {
                                formField.menuItems && formField.menuItems.map((role, i) =>
                                  <MenuItem key={i.name} value={role.name}>
                                    {role.displayName}
                                  </MenuItem>
                                )
                              }
                            </TextField>
                          )
                        );
                      })
                  }
                </div>
                <FormControlLabel
                  label={sameAsBilling ? 'Both Shipping & Billing Addresses Are Same' : 'Shipping Address Is Different'}
                  control={
                    <Switch checked={sameAsBilling} onChange={handleAddressChange} aria-label="TaxSwitch" />
                  }
                />
              </div>

              <div>
                <Typography variant="title" className={classes.title}>
                  {!sameAsBilling ? rightTitle : " "}
                </Typography>
                {(!sameAsBilling && shippingAddressProps) &&
                  shippingAddressProps
                    .map((formField, i) => {
                      return (!formField.isLeft &&
                        (formField.type === 'text' ?
                          <div>
                            <TextField
                              type={formField.type}
                              label={formField.label}
                              value={formField.value}
                              onChange={handleFieldChange(formField.fieldName, formField.validFieldName)}
                              className={classes.textField}
                              fullWidth
                              margin="normal"
                              error={formField.isValid === false}
                            />
                            {
                              formField.isValid === false &&
                              <FormHelperText className={classes.red} id="name-error-text" >
                                <span className={classes.errorFont}>
                                  Required Field
                                      </span>
                              </FormHelperText>
                            }
                          </div>
                          :
                          <TextField
                            select
                            label={!formField.value && formField.helperText}
                            className={classes.textField}
                            value={formField.value}
                            onChange={handleFieldChange(formField.fieldName)}
                            helperText={formField.helperText}
                            margin="normal"
                          >
                            {
                              formField.menuItems && formField.menuItems.map((role, i) =>
                                <MenuItem key={i.name} value={role.name}>
                                  {role.displayName}
                                </MenuItem>
                              )
                            }
                          </TextField>
                        )
                      );
                    })
                }
              </div>
            </form>

            <Button
              key={1}
              className={classes.button}
              type="submit"
              onClick={handleFormSubmit}>
              <SaveIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
              {isUpdate ? 'Update' : 'Save'}
            </Button>
          </Card>
        </Grid>
      </Grid>
    </div>
  );

export default withStyles(styles)(Form);
