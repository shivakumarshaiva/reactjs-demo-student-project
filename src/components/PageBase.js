import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import AppDrawer from './Drawer';
import AppBar from './AppBar';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  appFrame: {
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,

    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  'appBarShift-left': {
    marginLeft: drawerWidth,
  },
  'appBarShift-right': {
    marginRight: drawerWidth,
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 0,
  },
  hide: {
    display: 'none',
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  logo: {
    flexGrow: 1,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.primary,
    '@media (min-width: 481px)': {
      padding: theme.spacing.unit * 3,
    },
    padding: 10,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
});

class PageBase extends React.Component {
  state = {
    open: false,
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };
  dashBoard = (event) => {
    this.props.history.push('/dashboard');
  }
  userPage = (event) => {
    this.props.history.push('/users');
  }
  studentsPage = (event) => {
    this.props.history.push('/students');
  }
  teachersPage = (event) => {
    this.props.history.push('/teachers');
  }

  render() {
    const { classes, children } = this.props;

    return (
      <div className={classes.root}>
        <div className={classes.appFrame}>
          <AppBar open={this.state.open} handleDrawerOpen={this.handleDrawerOpen}/>
          <AppDrawer
            handleDrawerClose={this.handleDrawerClose}
            userPage={this.userPage}
            studentsPage={this.studentsPage}
            teachersPage={this.teachersPage}
            dashBoard={this.dashBoard}
            anchor={this.state.anchor}
            open={this.state.open}
          />
          <main className={classes.content}>
            <div className={classes.drawerHeader} />
            {children}
          </main>
        </div>
      </div>
    );
  }
}

PageBase.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(withRouter(PageBase));
