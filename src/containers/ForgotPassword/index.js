import React from 'react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import axios from "axios";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import LockIcon from '@material-ui/icons/Lock';
import Avatar from '@material-ui/core/Avatar';
import FormHelperText from '@material-ui/core/FormHelperText';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import LinearProgress from '../../components/LinearProgress';
import Notification from '../../components/Notifications/success';
import config from '../../config';

const styles = theme => ({
  main: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    alignItems: 'center',
    justifyContent: 'flex-start',
    background: 'url(http://www.forallworld.com/data/out/2/IMG_24278.jpg)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  avatar: {
    backgroundColor: theme.palette.primary.main,
    margin: '0 auto',
  },
  card: {
    minWidth: 300,
    margin: 'auto auto',
  },
  form: {
    padding: '1em 1em 1em 1em',
  },
  button: {
    marginTop: '20px',
    color: '#fff',
    width: '100%',
    borderRadius: '5px',
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  font: {
    fontSize: '.9rem',
    fontWeight: 300,
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamily,
    flexGrow: 1,
    width: '100%'
  },
});

class ForgotPassword extends React.Component {
  state = {
    userName: '',
    isLoading: false,
    isUserNameMissing: null,
  };


  handleFieldChange = (fieldName, isfieldValid = null) => (event) => {
    this.setState({
      [fieldName]: event.target.value,
      ...(isfieldValid && { [isfieldValid]: Boolean(event.target.value) }),
    });
  }

  componentWillMount() {
    const { location } = this.props;
    console.log('location :', location);

  }

  submitHandler = (event) => {
    event.preventDefault();
    const { history } = this.props;
    const { userName } = this.state
    const body = { username: userName};
    const self = this;

    this.setState({
      isLoading: true,
    })

    const URL = config.BASE_URL + 'forgot-password';
    axios({
      method: 'get',
      url: URL,
      params: body,
    }).then(function (res) {
      const isEmailExists = res.data;
      if(isEmailExists){
        self.setNotificationMessage('New Password sent to your Email!', 'success', false);
        setTimeout(() => history.push('/login'), 3000);
      }
      else {
        self.setNotificationMessage("Email Does not Exists", 'error', false);
      }
    }).catch(function (error) {
      console.log('error while logging in...', error, false);
      alert("Username password do not match");
    })
  }

  setNotificationMessage = (value, variant, isLoading = false) => {
    if(value){
      this.setState({ snackBarMessage: value, snackBarVariant: variant, isLoading: false });
    }
    else{
      this.setState({ snackBarMessage: null, snackBarVariant: null, isLoading: false });
    }
  }

  render() {
    const { theme, classes, } = this.props;
    const { userName,
      isUserNameMissing,
      snackBarMessage,
      snackBarVariant,
      isLoading,
    } = this.state

    return (
      <div className={classes.main}>
        <Notification
          message={snackBarMessage}
          variant={snackBarVariant}
          handleClose={() => this.setNotificationMessage(null)}
        />
        <Card className={classes.card} >
          <div style={{ backgroundColor: theme.palette.primary.main, height: '4px' }}>
            {isLoading && <LinearProgress />}
          </div>
          <CardContent style={{ paddingBottom: '0px' }}>
            <Avatar aria-label="Recipe" className={classes.avatar}>
              <LockIcon />
            </Avatar>
            <Typography variant="title" className={classes.font}>
              Enter your valid e-mail to receive password!
            </Typography>      
          </CardContent>
          <form className={classes.form} noValidate autoComplete="off">
            <TextField
              style={{ margin: '0 auto' }}
              id="name"
              label="User Name"
              value={userName}
              onChange={this.handleFieldChange('userName', 'isUserNameMissing')}
              margin="normal"
              fullWidth
              error={isUserNameMissing === false}
            /> <br />
            {
              isUserNameMissing === false
              && <FormHelperText className={classes.red} id="name-error-text">
                Please Enter UserName / E-mail
                </FormHelperText>
            }

            <Button
              className={classes.button}
              type="submit"
              id="createButton"
              disabled={isLoading}
              name="Submit"
              onClick={this.submitHandler}
            >
              Submit
            </Button>
          </form>
        </Card>
      </div>
    );
  }
}

ForgotPassword.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default (withStyles(styles, { withTheme: true })(withRouter(ForgotPassword)));