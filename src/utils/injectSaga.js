import React from "react";
import PropTypes from 'prop-types';

export default (saga) => (Component) => {
  class WrapperComponent extends React.Component {

    constructor(props) {
      super(props);
      this.saga = null;
    }
    componentWillMount() {
      this.saga = this.context.store.runSaga(saga);
    }

    componentWillUnmount() {
      if (this.saga) this.saga.cancel();
    }

    render() {
      return <Component {...this.props} />
    }

  }

  WrapperComponent.contextTypes ={
    store: PropTypes.object.isRequired,
  };

  return WrapperComponent;
}