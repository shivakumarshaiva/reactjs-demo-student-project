import React from 'react';
import PropTypes from 'prop-types';
import {compose} from 'redux';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import PageHeader, {HeaderButton} from '../../../components/PageHeader';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import Card from '@material-ui/core/Card';
// import {selectCustomers, selectCustomersTotal} from '../../../reducers/custoners/customerSelector';
import Table from '../../../components/Table'
import sagaHelper from '../../../sagas';
import {default as sagaInjector} from '../../../utils/injectSaga';
import { TEACHER_RESOURCE, deleteResource, list, RESOURCE_DELETE, RESOURCE_LIST, search,} from '../../../actions';
import NavigationBar from '../../../components/NavigationBar';
import {DELETE_SUCCESS} from "../../../utils/constonts";
import {selectMessage} from '../../../reducers/genericSelector';
import { isStringIncludes } from '../../../utils/commonFuctions';

class CustomerList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      order: 'desc',
      orderBy: 'updatedAt',
      selected: [],
      page: 1,
      rowsPerPage: 5,
      searchText: '',
    };
  }

  componentDidMount() {
    this.getCustomersList(({
      sort: `${this.state.orderBy},${this.state.order}`,
      page: `${this.state.page}`,
      limit: `${this.state.rowsPerPage}`
    }));
  }
  getCustomersList = (data = {}) => {
    // this.props.getCustomersRequest(data);
  }

  createSubmit = () => {
    this.props.history.push('/customers/create');
  }
  uploadSubmit = () => {
    this.props.history.push('/customers/upload');
  }

  handleChangePage = (event, page) => {
    this.setState({ page: page });
    this.setState({selected: []})
    if(this.state.searchText) {
      this.props.makeSearchRequest({
        description: this.state.searchText,
        page: `${page}`,
        size: `${this.state.rowsPerPage}`,
        sort: `${this.state.orderBy},${this.state.order}`,
      });
    } else {
      this.props.makePageChangeRequest({
        page: `${page}`,
        size: `${this.state.rowsPerPage}`,
        sort: `${this.state.orderBy},${this.state.order}`,
      })
    }
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
    if (this.state.searchText) {
      this.props.makeSearchRequest({
        description: this.state.searchText,
        page: `${this.state.page}`,
        size: `${event.target.value}`,
        sort: `${this.state.orderBy},${this.state.order}`,
      });
    } else {
      this.getCustomersList(({
        page: `${this.state.page}`,
        size: `${event.target.value}`,
        sort: `${this.state.orderBy},${this.state.order}`,
      }));
    }
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';
    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }
    this.setState({ order, orderBy }, () => this.props.makeSortingRequest({ 
      sort: `${property},${order}`,
      page: `${this.state.page}`,
      size: `${this.state.rowsPerPage}` 
    }));
  };

  makeSearchRequest = (event) => {
    event.preventDefault();
    this.props.makeSearchRequest({
      description: this.state.searchText,
      page: `${this.state.page}`,
      size: `${this.state.rowsPerPage}`
    });
  }

  makeDeleteAllRequest = (event) => {
    this.props.makeDeleteAllRequest({
      ids: this.state.selected.join(','),
      page: `${this.state.page}`,
      size: `${this.state.rowsPerPage}`
    })
    this.setState({ selected: [] });
    window.location = "/customers";
  }

  handleSelectAllClick = (event, checked) => {
    const{customers} = this.props;
    if (checked) {
      this.setState(state => ({ selected: customers && customers.map(n => n.id) }), () => console.log("selected all items", this.state.selected));
    } else {
      this.setState({ selected: [] }, () => console.log("unselected items", this.state.selected));
    }
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected && selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    this.setState({ selected: newSelected }, () => console.log("selected customs item", this.state.selected));
  };

  handleFieldChange = (event) => {
    this.setState({
      searchText: event.target.value,
    });
  }

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { total, customers, selectMessage } = this.props
    const { page, rowsPerPage, selected, orderBy, order, searchText } = this.state
    const isSuccess = isStringIncludes(selectMessage, DELETE_SUCCESS) 

    if (isSuccess) {
      this.props.getCustomersRequest({
        page: `${page}`,
        size: `${rowsPerPage}`,
        sort: `${orderBy},${order}`,
      })
    }

    let customerList = customers && customers.map(suggestion => ({
      id: suggestion.id,
      name: suggestion.name,
      email: suggestion.email,
      customerCode: suggestion.customerCode,
      description: suggestion.description,
      defaultShippingMethod: suggestion.defaultShippingMethod,
      defaultPaymentMethod: suggestion.defaultPaymentMethod,
    }))

    const tableData = {
      columns: [  
        { id: 'name', numeric: false, disablePadding: true, label: 'Name', enableSort: true },
        { id: 'email', numeric: false, disablePadding: true, label: 'Email', enableSort: true },
        { id: 'customerCode', numeric: false, disablePadding: false, label: 'Customer Code', enableSort: false },
        { id: 'description', numeric: false, disablePadding: false, label: 'Description', enableSort: false },
        { id: 'defaultShippingMethod', numeric: false, disablePadding: false, label: 'Shipping Method', enableSort: false },
        { id: 'defaultPaymentMethod', numeric: false, disablePadding: false, label: 'Payment Method', enableSort: false },
      ],
      edit: true,
      rowData: customerList,
      path: TEACHER_RESOURCE,
    };

    const actions = [
      { urlTemplate: "customers/{id}/edit", icon: <EditIcon />, toolTip: "Edit" }
    ]

    const createButton = <HeaderButton key="createButton" handler={this.createSubmit} icon={<AddIcon />} iconLabel="Create" toolTip="Create New Customer" />
    const pageHeader = <PageHeader title={"Teachers List"} headerButtons={[] } />

    return (
      <div>
        <NavigationBar
          isGoBack={true}
          isRefresh={true}
          headerButtons={[createButton]}
        />
        <Card>
          <Table
            actions={actions}
            pageHeader={pageHeader}
            tableData={tableData}
            total={total}
            handleChangePage={this.handleChangePage}
            handleChangeRowsPerPage={this.handleChangeRowsPerPage}
            page={page}
            rowsPerPage={rowsPerPage}
            handleSelectAllClick={this.handleSelectAllClick}
            isSelected={this.isSelected}
            handleClick={this.handleClick}
            handleRequestSort={this.handleRequestSort}
            selected={selected}
            orderBy={orderBy}
            order={order}
            handleFieldChange={this.handleFieldChange}
            makeSearchRequest={this.makeSearchRequest}
            searchText={searchText}
            deleteSelected={this.makeDeleteAllRequest}
          />
        </Card>
      </div>
      
    );
  }
}

CustomerList.propTypes = {
  customers: PropTypes.array,
  total:PropTypes.number,
  isLoading: PropTypes.bool,
};

const mapDispatchToProps = (dispatch) => ({
  makePageChangeRequest: (param) => dispatch(list(TEACHER_RESOURCE, param)),
  makeSearchRequest: (param) => dispatch(search(TEACHER_RESOURCE, param)),
  makeDeleteAllRequest: (param) => dispatch(deleteResource(TEACHER_RESOURCE, param)),
  makeSortingRequest: (param) => dispatch(list(TEACHER_RESOURCE, param)),
  getCustomersRequest: (param) => dispatch(list(TEACHER_RESOURCE, param)),
});

const mapStateToProps = createStructuredSelector({
  // customers: selectCustomers(),
  // total: selectCustomersTotal(),
  selectMessage: selectMessage(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  sagaInjector(sagaHelper(RESOURCE_LIST)),
  sagaInjector(sagaHelper(RESOURCE_DELETE)),
  withConnect,
)(withRouter(CustomerList));