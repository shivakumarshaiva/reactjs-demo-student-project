import React from 'react';
import { withRouter } from 'react-router-dom';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

class UserList extends React.Component {

  render() {
    const { tableData, isSelected, handleClick, actions,} = this.props;
    return (
      <TableBody>
        {tableData.rowData && tableData.rowData
          .map(n => {
            const isSelect = isSelected(n.id);
            return (
                <TableRow
                  hover
                  role="checkbox"
                  aria-checked={isSelect}
                  tabIndex={-1}
                  key={n.key}
                  selected={isSelect}
                >
                {!tableData.isNotDeletable &&  
                  <TableCell key={n.key} padding="checkbox">
                    <Checkbox key={n.key} checked={isSelect} onClick={event => handleClick(event, n.id)} />
                  </TableCell>
                }
                {tableData.columns && tableData.columns.map(column => {
                    return(
                      <TableCell numeric={column.numeric && column.numeric} key={n[`${column.key}`]}>
                        {n[`${column.id}`]}
                      </TableCell>
                    )
                  })
                }
                <TableCell key={n.key} >
                  {actions && actions.map( it => {
                    return (
                      it.urlTemplate && 
                      <a href={`${it.urlTemplate.replace("{id}", n["id"])}`}>
                        <Tooltip title={`${it.toolTip}`} placement="top">
                          <Button>
                            {it.icon}
                          </Button>
                        </Tooltip>
                      </a>)
                  })}
                </TableCell>
              </TableRow>
            );
          })}
      </TableBody>
    );
  }
}

export default (withRouter(UserList));
