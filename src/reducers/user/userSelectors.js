import { createSelector } from 'reselect';

const root = state => {
  return state.root.users;
}

const selectsUsers = () =>
  createSelector(root, userState => userState.list);

const selectUserTotal = () =>
  createSelector(root, userState => userState.total);

const selectUser = () =>
  createSelector(root, userState => userState.single);

const selectIsTokenExpired = () =>
  createSelector(root, userState => userState.isTokenExpired);

const selectIsLoading = () =>
  createSelector(root, userState => userState.isLoading);

const selectMessage = () => 
  createSelector(root, userState => userState.message);


export { 
  selectUser,
  selectsUsers, 
  selectUserTotal, 
  selectIsTokenExpired,
  selectIsLoading,
  selectMessage,
};