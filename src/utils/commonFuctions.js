export const currencyFormatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2,
});

export const phoneNumberFormater = (phone) => {
  if (phone) {
    const originalNumber = phone && phone.toString().replace(/[^0-9]/g, '');
    if (originalNumber && originalNumber.length === 10) {
      const formatedNumber = originalNumber && originalNumber.toString().replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
      return formatedNumber;
    }else{
      return originalNumber;
    }
  }
}

export const phoneDigitLength = function(phone) {
  if (phone) {
    const originalNumber = phone && phone.toString().replace(/[^0-9]/g, '');
    if (originalNumber && originalNumber.length === 10) {
      return true;
    } else {
      return false;
    }
  }
}

export const isStringIncludes = (content, word) => {
  if (content && (content.indexOf(word) > -1)) {
     return true;
  } else {
    return false;
  }
}

export const cleanDecimalNumber = function (number) {
  let filter = '';
  if (number && number !== 'undefined') {
    filter = number
    console.log('filter :', filter);
    const originalNumber = filter && filter.toString().replace(/[^-0-9.]/g, '');
    console.log('originalNumber :', originalNumber);
    return originalNumber;
  }
}

export const cleanIntegerNumber = function (number) {
  let filter = '';
  if (number &&  number !== 'undefined') {
    filter = number;
    const originalNumber = filter && filter.toString().replace(/[^-0-9]/g, '');
    return originalNumber;
  }
}

export const emailValidation = function(inputText) {
  var mailformat = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
  if ( inputText && inputText.match(mailformat)) {
    return true;
  }
  else {
    return false;
  }
}

export const intersection = function (a, b) {
  return [...new Set(a)].filter(v => new Set(b).has(v));
};
