import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import BackIcon from '@material-ui/icons/ArrowBackRounded';
import ReFresh from '@material-ui/icons/Refresh';
import Tooltip from '@material-ui/core/Tooltip';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  font: {
    color: '#fff',
    fontFamily: theme.typography.fontFamily,
  },
  button: {
    height:'100%',
    marginLeft:3,
    marginRight: 4,
    color: '#fff',
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  textField: {
    width: 500,
    margin: 0,
  },
});


class NavigationBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
    };
  }

  goBack = () => {
    this.props.history.goBack();
  }
  refresh = () => {
    window.location.reload();
  }

  render() {
    const { anchorEl } = this.state;
    const { classes, actions, isGoBack, isRefresh, textField, headerButtons } = this.props;

    return (
      <Card style={{ width: '100%', display: 'flex', marginBottom: '10px', backgroundColor:'rgb(247, 247, 247)'}}>
        <div style={{ width: '30%' }}>
          {
            isGoBack && 
            <Tooltip title='Go Back'>
                <Button
                key={1}
                onClick={this.goBack}
                >
                <BackIcon />
              </Button>
           </Tooltip>
          }
          {
            isRefresh &&
            <Tooltip title='ReFresh'>
              <Button
                key={1}
                onClick={this.refresh}
              >
                <ReFresh />
              </Button>
            </Tooltip>
          }
        </div>
          
      {
          <div style={{ display: textField && 'flex', width: '70%'}}>
            <div style={{ width: textField ? '50%' : '100%', height: '100%',textAlign: 'right', }}>
              {
                actions && actions.map(action => {
                  return (
                    <Tooltip title={action.toolTip ? action.toolTip : ''}>
                      <Button
                        disabled={action.isEnabled}
                        key={1}
                        className={action.isColor && classes.button}
                        onClick={action.method}
                      >
                        {action.icon ? action.icon : ''}
                        {action.lable ? action.lable : ''}
                      </Button>
                    </Tooltip>
                  );
                })
              }
            </div>
            {
              textField && 
              <div style={{ marginLeft: 14 }}>
                {
                  textField &&
                  <TextField
                    text
                    disabled={textField.isEnabled}
                    label={textField.lable}
                    value={textField.value}
                    onChange={textField.handleFieldChange(textField.field)}
                    className={classes.textField}
                    // helperText={textField.helperText}
                    fullWidth
                    margin="normal"
                  />
                }
              </div>
            }
        </div>
        }
        {headerButtons && headerButtons}
      </Card>
    );
  }
}
export default withStyles(styles, { withTheme: true })(withRouter(NavigationBar));
