import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import TablePagination from '@material-ui/core/TablePagination';
class Pagination extends React.Component {


  render() {
    const { count, page, rowsPerPage, handleChangePage, handleChangeRowsPerPage } = this.props;
    return (
        <TablePagination
          component="div"
          labelRowsPerPage="Page size:"
          count={count}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={handleChangePage }
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
    );
  }
}
Pagination.propTypes = {
  total: PropTypes.number
};

export default (withRouter(Pagination));