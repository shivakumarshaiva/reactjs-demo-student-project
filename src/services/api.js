import axios from "axios";
import configs from '../config';

export const CREATE         = 'CREATE';
export const UPDATE         = 'UPDATE';
export const DELETE         = 'DELETE';
export const GET_ONE        = 'GET_ONE';
export const LIST           = 'LIST';
export const SEARCH         = 'SEARCH';
export const UPLOAD         = 'UPLOAD';

// function that makes the api request and returns a Promise for response
export default function client(resource, type = 'GET_ONE', payload = {}, isMultipart = false) {
  let url = configs.BASE_URL +"/" + resource+"/";
  const token = localStorage.getItem('token');
  let method = '';
  let ids = '';
  let requestConfig = {};
  var headers = '';

  if (type === 'UPLOAD') {
    url = configs.BASE_URL + "/ws/";
    headers = {
      'content-type': 'binary/octet-stream',
      // 'Authorization': `Bearer ${token}`,
    }
  } else {
    headers = {
      'content-type': 'application/json',
    };
  }

  switch (type) {

    case GET_ONE:
      method = 'get';
      url = url + payload
    break;
    
    case LIST:
      method = 'get';
      requestConfig = { params: payload}
    break;

    case CREATE: 
      method = 'post';
      url = url + "create";
      requestConfig = { data: payload }
    break;

    case UPDATE:
      method = 'post';
      url = url + 'update/' + payload._id;
      requestConfig = { data: payload }
    break;

    case UPLOAD:
      method = 'post';
      url = url + "upload";
      requestConfig = { data: payload }
    break;

    case DELETE:
      method = 'delete';
      ids = payload.ids
      requestConfig = { params: { ids: ids } }
    break;

    case SEARCH:
      method = 'get';
      url = url + 'search/';
      requestConfig = { params: payload }
    break;
  }

  requestConfig = {
    url,
    method,
    headers,
    ...requestConfig,
  }

  console.log('final req requestConfig :', requestConfig);
  
  return axios(requestConfig);
}