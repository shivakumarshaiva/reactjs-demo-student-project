import React from 'react';
import { Fragment } from "react";
import UserAvatar from 'react-avatar';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import IconButton from '@material-ui/core/IconButton';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  font: { 
    color: '#fff', 
    fontFamily: theme.typography.fontFamily, 
  },
});


class ProfileStatus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
    };
  }

  handleChange = (event, checked) => {
    this.setState({ auth: checked });
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  reset = () => {
    this.props.history.push('/reset')
  }
  logout = () => {
    window.localStorage.removeItem('token');
    window.localStorage.removeItem('user');
    setTimeout(() =>localStorage.clear() , 1000);
    this.props.history.push('/login')
  };
  render () {
    const { anchorEl } = this.state;
    const {classes} = this.props;
    const profile = Boolean(anchorEl);
    return (
      <Fragment>
        <span className={classes.font}>
          {localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')).email.toUpperCase() : ""} 
        </span>
        <IconButton
          aria-owns={profile ? 'menu-appbar' : null}
          aria-haspopup="true"
          onClick={this.handleMenu}
          color="inherit"
        >
          <UserAvatar  
            googleId="10n0AIlacQjz2EQGs2Hb3ClLNzbySDfr0"
            round={true} 
            name={localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')).sub : ""}
            size="35" 
            // src='https://static1.squarespace.com/static/56a1a14b05caa7ee9f26f47d/t/57285c19044262e74cfce0a0/1462262813062/' 
          />
        </IconButton>
        <Menu
          id="menu-appbar"
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={profile}
          onClose={this.handleClose}
        >
          <MenuItem onClick={this.reset}>Change Password</MenuItem>
          <MenuItem onClick={this.logout}>Logout</MenuItem>
        </Menu>
      </Fragment>
    );
  }
}
export default withStyles(styles, { withTheme: true })(withRouter(ProfileStatus));
