import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import Form from '../../../components/Form';
import {
    getOne,
    RESOURCE_GET_ONE,
    RESOURCE_LOOKUP,
    RESOURCE_UPDATE,
    update,
    USERS_RESOURCE,
} from '../../../actions';
// import {selectUser,} from '../../../reducers/user/userSelectors'
import {createStructuredSelector} from 'reselect';
import sagaHelper from '../../../sagas';
import {default as sagaInjector} from '../../../utils/injectSaga';
import Alert from '../../../components/Alert';
import PageHeader from '../../../components/PageHeader';
import ListIcon from '@material-ui/icons/List';
import NavigationBar from '../../../components/NavigationBar';
import Card from '@material-ui/core/Card';
import {emailValidation, phoneDigitLength, phoneNumberFormater as phoneFormatter} from '../../../utils/commonFuctions';
import {UPDATE_SUCCESS} from "../../../utils/constonts"
import {selectMessage} from '../../../reducers/genericSelector';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    width: 400,
    margin: '0 auto',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  menu: {
    width: 300,
  },
});

class UserUpdate extends React.Component {
  constructor() {
    super();
    this.state = {
      id:'',
      firstName: '',
      lastName: '',
      username:'',
      email: '',
      password: '',
      role: null,
      contactNumber: '',
      employeeId: '',
      address:{},
      userRoles: [],

      city: '',
      attn: '',
      line1: '',
      line2: '',
      zip: '',
      phone: '',

      file: null,

      isUserRoleValid: null,
      isAddressValid: null,
      isCityValid: null,
      isStreetValid: null,
      isPassword: null,
      isCreated: null,
      isEmailValid: null,
      isUserNameValid: null,
      isFirstNameValid: null,
      isLastNameValid: null,
      showFormInvalid: false,

      isDilogOpen: false,
      dilogMessage: '',
    };
  }

  handleFieldChange = (fieldName, isfieldValid = null) => (event) => {
    if (fieldName === "phone" || fieldName === "contactNumber") {
      const onlyNums = event.target && event.target.value.replace(/[^0-9]/g, '');
      if (onlyNums.length < 10) {
        this.setState({ [fieldName]: onlyNums });
      } else if (onlyNums && onlyNums.length === 10) {
        const number = onlyNums && onlyNums.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
        this.setState({ [fieldName]: number });
      }
    } else { 
      this.setState({
        [fieldName]: event.target.value,
        ...(isfieldValid && { [isfieldValid]: Boolean(event.target.value) }),
      });
    }
  }

  createdCheck() {
    this.setState({ isCreated: this.props.user ? true : false })
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.makeUserGetRequest(id);
    // this.props.makeGetLookupRequest();
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.user) {
      const {
        id, firstName, lastName, username, password, address, roles, contactNumber, employeeId, email
      } = nextProps.user;
      this.setState({
        id: id,
        firstName: firstName,
        lastName: lastName,
        email,
        username,
        password: password,
        contactNumber: contactNumber && contactNumber.replace(/[^0-9]/g, ''),
        employeeId: employeeId,
        role: roles && roles[0].id,
        userRoles: nextProps.lookup.userRoles,

        attn: address && address.attn,
        line1: address && address.line1,
        line2: address && address.line2,
        zip: address && address.zip,
        phone: address && address.phone && address.phone.replace(/[^0-9]/g, ''),
        city: address && address.city,
      })
    }
  }

  handleFormSubmit = (event) => {
    event.preventDefault();
    const {
      id, firstName, lastName, email, role, city, attn, line1, line2, 
      zip, file, phone, contactNumber, employeeId, username,
    } = this.state;
    if (!firstName) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter User First Name',
      });
      return;
    }
    if (!username) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter User Name',
      });
      return;
    }
    if (!email) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Contact Email',
      });
      return;
    }
    if (email && !emailValidation(email)) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'You Are Antered Invalid Email!!',
      });
      return;
    }
    if (phone && !phoneDigitLength(phone)) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Address\'s  10 Digit Contact Number',
      });
      return;
    }
    if (contactNumber && !phoneDigitLength(contactNumber)) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Enter Personal 10 Digit Contact Number',
      });
      return;
    }
    if (!role) {
      this.setState({
        isDilogOpen: true,
        dilogMessage: 'Please Select User Role',
      });
      return;
    }

    const payload = {
      id,
      firstName,
      lastName,
      username,
      email,
      contactNumber,
      employeeId,
      city,
      role,
      address: {
        city,
        attn,
        email,
        line1,
        line2,
        zip,
        phone,
      },
      roles: [
        {
          id: role
        }
      ]
    };
    const formData = new FormData();
    { file && formData.append('image', file) }
    formData.append(USERS_RESOURCE, JSON.stringify(payload));
    this.props.makeUserUpdateRequest(formData, this.props.history, true);
  }

  onChange = (e) => {
    this.setState({ file: e.target.files[0] })
  }

  snackClose = () => {
    this.setState({ open: false });
  };

  usersListPage = () => {
    this.props.history.push('/users');
  }

  dilogHandle = () => {
    this.setState({
      isDilogOpen: false,
    });
  }
  
  render() {
    const { firstName, lastName, email, role, isEmailValid, isFirstNameValid, username, isDilogOpen, dilogMessage,
    isUserNameValid, city, attn, line1, line2, zip, phone, contactNumber, employeeId, userRoles
    } = this.state;

    const { selectMessage } = this.props

    if (selectMessage && selectMessage.indexOf(UPDATE_SUCCESS) > -1) {
      this.props.history.push("/users");
    }

    const formProps = [
      //user general details
      { type: 'text', label: 'First Name', value: firstName, fieldName: 'firstName', isValid: isFirstNameValid,  validFieldName: 'isFirstNameValid', isLeft: true },
      { type: 'text', label: 'Last Name', value: lastName, fieldName: 'lastName', isLeft: true },
      { type: 'text', label: 'User Name', value: username, isValid: isUserNameValid, fieldName: 'username', validFieldName: 'isUserNameValid', isLeft: true },
      { type: 'text', label: 'Email', value: email, isValid: isEmailValid, fieldName: 'email', validFieldName: 'isEmailValid', isLeft: true },
      { type: 'text', label: 'Contact Number', value: contactNumber && phoneFormatter(contactNumber), fieldName: 'contactNumber', isLeft: true },
      { type: 'text', label: 'Employee Id', value: employeeId, fieldName: 'employeeId', isLeft: true },
      { type: 'select', label: 'User Role', value: role, fieldName: 'role', menuItems: userRoles, helperText: "Select User Role", isLeft: true, },
      //address details
      { type: 'text', label: 'attn', value: attn, fieldName: 'attn', isLeft: false },
      { type: 'text', label: 'line1', value: line1, fieldName: 'line1', isLeft: false },
      { type: 'text', label: 'line2', value: line2, fieldName: 'line2', isLeft: false },
      { type: 'text', label: 'city', value: city, fieldName: 'city', isLeft: false },
      { type: 'text', label: 'zip', value: zip, fieldName: 'zip', isLeft: false },
      { type: 'text', label: 'Phone', value: phone && phoneFormatter(phone), fieldName: 'phone', isLeft: false },

    ];

    const actions = [
      {
        toolTip: "User List",
        icon: <ListIcon />,
        method: this.usersListPage,
      }
    ]
    const alert = <Alert isDilogOpen={isDilogOpen} dilogMessage={dilogMessage} dilogHandle={this.dilogHandle} />
    return (
      <div>
        {isDilogOpen && alert}
        <NavigationBar
          isGoBack={true}
          isRefresh={true}
          actions={actions}
        />
        <Card>
          <PageHeader title={"Update User"} headerButtons={[]} />
          <Form
            formProps={formProps}
            handleFieldChange={this.handleFieldChange}
            isUpdate={false}
            handleFormSubmit={this.handleFormSubmit}
            // isFileUpload={true}
            // fileTitle={"Upload Profile Photo"}
            onChange={this.onChange}
            rightTitle={"Address Details"}
            leftTitle={"General Information"}
            isUpdate={true}
          />
        </Card>
      </div>
    );
  }
}

UserUpdate.propTypes = {
  user: PropTypes.object,
  roles: PropTypes.object,
  lookup: PropTypes.object,
};

const mapDispatchToProps = dispatch => ({
  makeUserGetRequest: (id) => dispatch(getOne(USERS_RESOURCE, id)),
  makeUserUpdateRequest: (payload, history, isMultipart) => dispatch(update(USERS_RESOURCE, payload, history, isMultipart)),
});

const mapStateToProps = createStructuredSelector({
  // user: selectUser(),
  selectMessage: selectMessage(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  sagaInjector(sagaHelper(RESOURCE_UPDATE)), 
  sagaInjector(sagaHelper(RESOURCE_GET_ONE)),
  sagaInjector(sagaHelper(RESOURCE_LOOKUP)),
  withConnect,
)(withStyles(styles)(withRouter(UserUpdate)));
