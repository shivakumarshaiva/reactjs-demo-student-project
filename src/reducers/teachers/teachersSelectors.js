import { createSelector } from 'reselect';

const root = state => {
  return state.root.teachers;
}

const selectTeachers = () =>
  createSelector(root, teacherState => teacherState.list);

const selectTeachersTotal = () =>
  createSelector(root, teacherState => teacherState.total);

const selectTeacher = () =>
  createSelector(root, teacherState => teacherState.single);

export {
  selectTeacher,
  selectTeachers,
  selectTeachersTotal,
};