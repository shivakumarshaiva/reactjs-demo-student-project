import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import axios from "axios";
import jwt from 'jwt-decode';
import LinearProgress from '../../components/LinearProgress';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import LockIcon from '@material-ui/icons/Lock';
import Avatar from '@material-ui/core/Avatar';
import FormHelperText from '@material-ui/core/FormHelperText';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Button from '@material-ui/core/Button';
import config from '../../config';
import Notification from '../../components/Notifications/success';

const styles = theme => ({
  main: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    alignItems: 'center',
    justifyContent: 'flex-start',
    background: 'url(http://www.forallworld.com/data/out/2/IMG_24278.jpg)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  body :{
    margin: 0,
  },
  avatar: {
    backgroundColor: theme.palette.primary.main,
    margin: '0 auto',
    },
  card: {
    minWidth: 300,
    margin: 'auto auto',
  },
  form: {
    padding: '1em 1em 1em 1em',
  },
  button: {
    marginTop: '20px',
    color: '#fff',
    width: '100%',
    borderRadius: '5px',
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  forgotButton: {
    marginTop: '20px',
    color: '#fff',
    width: '50%',
    borderRadius: '5px',
    backgroundColor: theme.palette.primary.light,
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  font: {
    fontSize: '.9rem',
    fontWeight: 300,
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamily,
    flexGrow: 1,
  },
});

class Reset extends React.Component {
  state = {
    oldPassword: '',
    newPassword: '',
    token: '',
    role: '',
    snackBarMessage: null,
    snackBarVariant: null,
    showPassword: false,
    isOldPassword: null,
    isNewPassword: null,
    isLoading: false,
  };

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };

  handleFieldChange = (fieldName, isfieldValid = null) => (event) => {
    this.setState({
      [fieldName]: event.target.value,
      ...(isfieldValid && { [isfieldValid]: Boolean(event.target.value) }),
    });
  }

  componentWillMount() {
    const { location } = this.props;
  }

  setNotificationMessage = (value, variant, isLoading = false) => {
    if(value){
      this.setState({ snackBarMessage: value, snackBarVariant: variant, isLoading: false});
    }
    else{
      this.setState({ snackBarMessage: null, snackBarVariant: null, isLoading: false});
    }
  }

  submitHandler = (event) =>  {
    event.preventDefault();
    const { setNotificationMessage, setLoadingState } = this;
    const { history } = this.props;
    const { oldPassword, newPassword,} = this.state
    const userId = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')).id : "";
    const body = {
      id: userId,
      oldPassword: oldPassword, 
      newPassword: newPassword 
    };

    const userToken = localStorage.getItem('token');
    const URL = config.BASE_URL +'reset';

    this.setState({
      isLoading: true,
    })
    axios({
      method: 'post',
      url: URL,
      data: body,
      // headers: {
      //   'Authorization': `Bearer ${userToken}`,
      // }
    }).then(function(res) {
      console.log('Response : ', res);
      if (res.data){
        setNotificationMessage('Password changed successfully!', 'success', false);
        setTimeout(() => history.push('/login'), 3000);
      } else{
        setNotificationMessage('Password changed Failed!', 'error', false);
      }
    }).catch(function (error) {
      setNotificationMessage('Error reseting password', 'error', false);
      console.log('Error reseting password', error);
    })
  }

  render() {
    const { theme, classes, className } = this.props;
    const { 
      oldPassword , 
      newPassword ,
      showPassword,
      isOldPassword,
      isNewPassword, 
      snackBarMessage,
      snackBarVariant,
      isLoading,
    } = this.state

    return (
      <div className={classes.main}>
        <Notification
          message={snackBarMessage}
          variant={snackBarVariant}
          handleClose={() => this.setNotificationMessage(null)}
        />
        <Card className={classes.card} >
          <div style={{ backgroundColor: theme.palette.primary.main, height: '4px' }}>
            {isLoading && <LinearProgress />}
          </div>
          <CardContent style={{ paddingBottom: '0px' }}>
            <Avatar aria-label="Recipe" className={classes.avatar}>
              <LockIcon />
            </Avatar>
            <span className={classes.font}>
             Enter your old password and new password!
            </span>
          </CardContent>
          <form className={classes.form} noValidate autoComplete="off">              
            <TextField
              style={{margin:'0 auto'}}
              id="name"
              label="Old Password"
              value={oldPassword}
              onChange={this.handleFieldChange('oldPassword', 'isOldPassword')}
              margin="normal"
              fullWidth
              error={isOldPassword === false}
            /> <br/>
            {
              isOldPassword === false
              && <FormHelperText className={classes.red} id="name-error-text">
                    Old Password Is Required
                </FormHelperText>
            }
            <FormControl style={{width:'100%'}}>
              <InputLabel htmlFor="adornment-password">New Password</InputLabel>
                <Input
                  id="adornment-password"
                  type={showPassword ? 'text' : 'password'}
                  value={newPassword}
                  onChange={this.handleFieldChange('newPassword', 'isNewPassword')}
                  fullWidth
                  error={isNewPassword === false}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="Toggle password visibility"
                        onClick={this.handleClickShowPassword}
                        onMouseDown={this.handleMouseDownPassword}
                      >
                        {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              {
                isNewPassword === false
                && <FormHelperText className={classes.red} id="name-error-text">
                  New Password Is Required
                </FormHelperText>
              }
              </FormControl>
      
            <Button
              className={classes.button}
              type="submit"
              id="createButton"
              name="Submit"
              disabled={isLoading}
              onClick={this.submitHandler}
              >
              Reset
            </Button>
          </form>
        </Card>
      </div>
    );
  }
}

Reset.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default (withStyles(styles, { withTheme: true })(withRouter(Reset)));