import { createSelector } from 'reselect';

const root = state => {
  return state.root.students;
}

const selectStudents = () =>
  createSelector(root, studentState => studentState.list);

const selectStudentsTotal = () =>
  createSelector(root, studentState => studentState.total);

const selectStudent = () =>
  createSelector(root, studentState => studentState.single);

export {
  selectStudent,
  selectStudents,
  selectStudentsTotal,
};